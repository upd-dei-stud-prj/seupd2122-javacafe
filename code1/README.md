# Code directory #
Main directory for the code part of the project

### Organisation of the `code` folder ###

This folder is structured in the following way:
```
code/                       --> this folder
├─ target/                  --> java binaries (***)
├─ runs/                    --> run files (***)
├─ data/                    --> datasets (***)
│  ├─ args_processed.csv    --> Touché task 1 dataset (***)
├─ src/                     
│  ├─ main/
│  │  ├─ java/it/unipd/dei/javacafe/
│  │  │  ├─ index/          --> Index creation
│  │  │  ├─ parse/          --> Parser creation
│  │  │  │  ├─ ArgsParser.java     --> Src file of parser for args.me corpus
│  │  │  │  ├─ DocumentParser.java --> Iterable parser abstract class
│  │  │  │  ├─ ParsedDocument.java --> Represents a doc of args.me corpus
│  │  │  │  ├─ package-info.java
│  │  │  ├─ search/         --> Searcher creation
│  │  ├─ python/            --> .py source files
├─ README.md                --> this file
├─ pom.xml/                 --> maven project file
├─ .gitignore/              --> git ignore settings
```
[Source](https://ascii-tree-generator.com/)    
`(***)` : **folder/files ignored by git**

### Clone ###
`git clone https://bitbucket.org/upd-dei-stud-prj/seupd2122-javacafe/src/master/ `
### Build ###
`mvn`
TODO
### Run ###
To test parser: run main class in parse package after editing filepath property.

[//]: # (TODO piú info)