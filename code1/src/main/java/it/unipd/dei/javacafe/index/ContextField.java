package it.unipd.dei.javacafe.index;

import it.unipd.dei.javacafe.parse.ArgsFields;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;


public class ContextField extends Field {

    private static final FieldType CONTEXT_TYPE = new FieldType();

    static {
        CONTEXT_TYPE.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        CONTEXT_TYPE.setTokenized(true);
        CONTEXT_TYPE.setStored(false);
    }

    public ContextField(final String value) {
        super(ArgsFields.CONTEXT, value, CONTEXT_TYPE);
    }

}
