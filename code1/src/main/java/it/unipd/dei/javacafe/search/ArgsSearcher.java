package it.unipd.dei.javacafe.search;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import it.unipd.dei.javacafe.ParameterConfig;
import it.unipd.dei.javacafe.parse.ArgsFields;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.benchmark.quality.QualityQuery;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.wordnet.SynonymMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class ArgsSearcher {
    private static final class TOPIC_FIELDS {
        public static final String NUMBER = "number";
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
        public static final String NARRATIVE = "narrative";
    }

    private final String runID;
    private final PrintWriter run;
    private final PrintWriter json_run;
    private final IndexReader reader;
    private final IndexSearcher searcher;
    private final List<QualityQuery> topics;
    private final QueryParser qp;
    private final int maxDocsRetrieved;
    private long elapsedTime = Long.MIN_VALUE;
    private final SynonymMap synonymMap;

    public ArgsSearcher(final Analyzer analyzer, final Similarity similarity, final String indexPath,
                        final String topicsFile, final int expectedTopics, final String runID, final String runPath,
                        final int maxDocsRetrieved, final SynonymMap synonymMap) {
        if (analyzer == null) {
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (indexPath == null) {
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }

        final Path indexDir = Paths.get(indexPath);
        if (!Files.isReadable(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be read.", indexDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(indexDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to search the index.",
                    indexDir.toAbsolutePath()));
        }

        try {
            reader = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index reader for directory %s: %s.",
                    indexDir.toAbsolutePath(), e.getMessage()), e);
        }

        searcher = new IndexSearcher(reader);
        searcher.setSimilarity(similarity);

        if (topicsFile == null) {
            throw new NullPointerException("Topics file cannot be null.");
        }

        if (topicsFile.isEmpty()) {
            throw new IllegalArgumentException("Topics file cannot be empty.");
        }

        this.synonymMap = synonymMap;

        try {
            topics = new ArrayList<>();
            XmlMapper topicsMapper = new XmlMapper();
            List<Map<String, String>> topicsList = topicsMapper.readValue(new File(topicsFile), new TypeReference<>() {
            });
            for (Map<String, String> topic : topicsList) {
                String queryID = topic.get(TOPIC_FIELDS.NUMBER);
                Map<String, String> values = new HashMap<>();
                values.put(TOPIC_FIELDS.TITLE, topic.get(TOPIC_FIELDS.TITLE));
                //values.put(TOPIC_FIELDS.DESCRIPTION, topic.get(TOPIC_FIELDS.DESCRIPTION));
                //values.put(TOPIC_FIELDS.NARRATIVE, topic.get(TOPIC_FIELDS.NARRATIVE));
                topics.add(new QualityQuery(queryID, values));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to process topic file %s: %s.", topicsFile, e.getMessage()), e);
        }

        if (expectedTopics <= 0) {
            throw new IllegalArgumentException(
                    "The expected number of topics to be searched cannot be less than or equal to zero.");
        }

        if (topics.size() != expectedTopics) {
            System.out.printf("Expected to search for %s topics; %s topics found instead.", expectedTopics,
                    topics.size());
        }

        //TODO why do i need a default field, and what will be the name, one of the index fields?
        //qp = new QueryParser(ArgsFields.PREMISES_FIELDS.TEXT, analyzer);
        //qp = new QueryParser(ArgsFields.CONTEXT, analyzer);
        qp = new QueryParser(ParameterConfig.LUCENE_DOC_FIELD_NAME, analyzer);
        //String[] indexed_fields = {ArgsFields.PREMISES_FIELDS.TEXT,ArgsFields.CONCLUSION, ArgsFields.SENTENCES_FIELDS.SENT_TEXT};
        //qp = new MultiFieldQueryParser(indexed_fields, analyzer);
        //qp = new QueryParser(null, analyzer);

        if (runID == null) {
            throw new NullPointerException("Run identifier cannot be null.");
        }

        if (runID.isEmpty()) {
            throw new IllegalArgumentException("Run identifier cannot be empty.");
        }

        this.runID = runID;


        if (runPath == null) {
            throw new NullPointerException("Run path cannot be null.");
        }

        if (runPath.isEmpty()) {
            throw new IllegalArgumentException("Run path cannot be empty.");
        }

        final Path runDir = Paths.get(runPath);
        if (!Files.isWritable(runDir)) {
            throw new IllegalArgumentException(
                    String.format("Run directory %s cannot be written.", runDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(runDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to write the run.",
                    runDir.toAbsolutePath()));
        }

        Path runFile = runDir.resolve(ParameterConfig.DOCUMENTS_RUN_FILE);
        Path json_runFile = runDir.resolve(ParameterConfig.JSON_RUN_FILE);
        try {
            run = new PrintWriter(Files.newBufferedWriter(runFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING,
                    StandardOpenOption.WRITE));
            json_run = new PrintWriter(Files.newBufferedWriter(json_runFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING,
                    StandardOpenOption.WRITE));
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to open run file %s: %s.", runFile.toAbsolutePath(), e.getMessage()), e);
        }

        if (maxDocsRetrieved <= 0) {
            throw new IllegalArgumentException(
                    "The maximum number of documents to be retrieved cannot be less than or equal to zero.");
        }

        this.maxDocsRetrieved = maxDocsRetrieved;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    public void search() throws IOException, ParseException {

        int MAX_SYNONYMS = 1;

        // the start time of the searching
        System.out.printf("%n#### Start searching ####%n");
        final long start = System.currentTimeMillis();
        final Set<String> fields = new HashSet<>();
        fields.add(ArgsFields.ID);

        //il use this analyzer just to stem the query to get synonyms
        /*
        Analyzer k_stem_analyzer = CustomAnalyzer.builder()
                .withTokenizer(StandardTokenizerFactory.class)
                .addTokenFilter(LowerCaseFilterFactory.class)
                .addTokenFilter(StopFilterFactory.class)
                .addTokenFilter(KStemFilterFactory.class)
                .build();

        QueryParser topic_query_parser = new QueryParser(ArgsFields.PREMISES_FIELDS.TEXT, k_stem_analyzer);
         */

        Map<String, Set<Integer>> first_run = new HashMap<>();
        try {
            int topic_index = 0;
            for (QualityQuery topic : topics) {
                topic_index++;
                String title = topic.getValue(TOPIC_FIELDS.TITLE);
                Integer title_length = title.length();
                BooleanQuery.Builder bqb = new BooleanQuery.Builder();
                /*
                if (synonymMap != null) {
                    //run queryparser on title to get the analyzer running
                    //this can be done with a custom analyzer
                    title = topic_query_parser.parse(QueryParserBase.escape(title)).toString(ArgsFields.PREMISES_FIELDS.TEXT);
                    String[] topic_title_words = title.split(" ");
                    StringBuilder stringBuilder = new StringBuilder();
                    for (String word : topic_title_words) {
                        String[] synonyms = synonymMap.getSynonyms(word);
                        if (synonyms.length >= 1) {
                            word = synonyms[0];
                        }
                        stringBuilder.append(word);
                        stringBuilder.append(" ");
                    }
                    title = stringBuilder.toString();
                }

                 */
                bqb.add(qp.parse(QueryParserBase.escape(title)), BooleanClause.Occur.SHOULD);
                //bqb.add(qp.parse(QueryParserBase.escape(topic.getValue(TOPIC_FIELDS.DESCRIPTION))), BooleanClause.Occur.SHOULD);
                Query q = bqb.build();
                ScoreDoc[] scoreDocs = searcher.search(q, maxDocsRetrieved).scoreDocs;
                for (int i = 0, n = scoreDocs.length; i < n; i++) {
                    Document document = reader.document(scoreDocs[i].doc, fields);
                    String docID = document.get(ArgsFields.ID);
                    //this output is actually useless, but let's just keep it
                    run.printf(Locale.ENGLISH, "%s\t%s\t%s\t%d\t%.6f\t%s%n", topic.getQueryID(), "Q0", docID, i, scoreDocs[i].score, runID);
                    //will save this information into this temporary map, just to put it into a json file
                    //this will be used on the second index, to index only the sentences of the best documents per query
                    if (i < ParameterConfig.MAX_DOCS_RETRIEVED_FIRST_RUN) { //is this check redundant now?
                        Set<Integer> set = first_run.get(docID);
                        if (set == null) {
                            set = new HashSet<>();
                        }
                        set.add(topic_index);
                        first_run.put(docID, set);
                    }
                }
                run.flush();
            }
            //oh come on man!!!!!!
            //this didn't escape fkn quotes, and needed to add .writer() to it. geez
            //json_run.printf(new ObjectMapper().writeValueAsString(first_run));
            json_run.printf(new ObjectMapper().writer().writeValueAsString(first_run));
            json_run.flush();
        } finally {
            run.close();
            json_run.close();
            reader.close();
        }

        elapsedTime = System.currentTimeMillis() - start;
        System.out.printf("%d topic(s) searched in %d seconds.", topics.size(), elapsedTime / 1000);
        System.out.printf("#### Searching complete ####%n");
    }

    public static void main(String[] args) throws Exception {

        String topics = ParameterConfig.TOPIC_PATH;

        SynonymMap synonymMap = null;
        if (ParameterConfig.ENABLE_SYNONYMS) {
            synonymMap = new SynonymMap(new FileInputStream(ParameterConfig.SYNONYM_FILE_PATH));
        }

        final String indexPath = ParameterConfig.INDEX_PATH;
        final String runPath = ParameterConfig.EXPERIMENT_PATH;
        final String runID = ParameterConfig.RUN_ID;

        final int maxDocsRetrieved = ParameterConfig.MAX_DOCS_RETRIEVED_FIRST_RUN;

        final Analyzer analyzer = ParameterConfig.analyzer;

        final int expected_topics = ParameterConfig.EXPECTED_TOPICS;

        Similarity similarity = ParameterConfig.similarity;

        ArgsSearcher searcher = new ArgsSearcher(analyzer, similarity, indexPath, topics, expected_topics, runID, runPath, maxDocsRetrieved, synonymMap);
        searcher.search();
    }
}