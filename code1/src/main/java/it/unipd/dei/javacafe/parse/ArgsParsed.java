package it.unipd.dei.javacafe.parse;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;
import java.util.Map;

/**
 * Represents a parsed document to be indexed.
 *
 * @author Giovanni Foti (giovanni.foti@studenti.unidp.it)
 * @version 1.00
 * @since 1.00
 */
public class ArgsParsed {

    private final String id;
    private final String conclusion;
    private final List<Map<String,?>> premises;
    private final Map<String,?> context;
    private final List<Map<String,?>> sentences;

    public ArgsParsed(final String id, final String conclusion, final List<Map<String,?>> premises, final Map<String,?> context, final List<Map<String,?>> sentences) {
        if(id == null){
            throw new NullPointerException("Document identifier cannot be null.");
        }
        if(id.isEmpty()){
            throw new NullPointerException("Document identifier cannot be empty.");
        }

        this.id = id;
        this.conclusion = conclusion;
        this.premises = premises;
        this.context = context;
        this.sentences = sentences;
    }


    public String getId() {
        return id;
    }
    public String getConclusion() {return conclusion;}


    //multiple premises? Seems that there aren't multiple premises!
    public String getPremisesText(){
        try {
            return (String)premises.get(0).get(ArgsFields.PREMISES_FIELDS.TEXT);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Map<String,?> getContext(){
        return context;
    }

    public String getPremisesStance(){
        try {
            return (String)premises.get(0).get(ArgsFields.PREMISES_FIELDS.STANCE);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String,?>> getSentences(){
        return sentences;
    }

    public String getSentenceText(){
        try {
            return (String)sentences.get(0).get(ArgsFields.SENTENCES_FIELDS.SENT_TEXT);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getContextSourceText(){
        try {
            return (String) context.get(ArgsFields.CONTEXT_FIELDS.SOURCE_TEXT);
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves the "title" of the context field
     * Sometimes there's a "discussionTitle" field and sometimes a "topic" field
     */
    public String getContextTitle() {
        String contextDiscussionTitle = getContextDiscussionTitle();
        String contextTopic = getContextTopic();
        if (contextDiscussionTitle != null) return contextDiscussionTitle;
        if (contextTopic != null) return contextTopic;
        return "";
    }

    private String getContextDiscussionTitle(){
        try {
            return ((String) context.get(ArgsFields.CONTEXT_FIELDS.DISCUSSION_TITLE));
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private String getContextTopic(){
        try {
            return ((String) context.get(ArgsFields.CONTEXT_FIELDS.DISCUSSION_TITLE));
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }





    @Override
    public final String toString() {
        ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("identifier", id)
                .append("conclusion", conclusion)
                .append("premises", premises)
                .append("context", context)
                .append("sentences", sentences);
        return toStringBuilder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return (this == obj) || ((obj instanceof ArgsParsed) && id.equals(((ArgsParsed) obj).id));
    }

    @Override
    public final int hashCode() {
        return 41 * super.hashCode();
    }
}
