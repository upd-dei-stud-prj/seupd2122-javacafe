package it.unipd.dei.javacafe.search;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import it.unipd.dei.javacafe.ParameterConfig;
import it.unipd.dei.javacafe.parse.ArgsFields;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.benchmark.quality.QualityQuery;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.queryparser.simple.SimpleQueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.wordnet.SynonymMap;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class SentenceSearcher {
    private static final class TOPIC_FIELDS {
        public static final String NUMBER = "number";
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
        public static final String NARRATIVE = "narrative";
    }

    private final String runID;
    private final PrintWriter run;
    private final PrintWriter touche_run;
    private final IndexReader reader;
    private final IndexSearcher searcher;
    private final List<QualityQuery> topics;
    private SimpleQueryParser sqp;
    private final int maxDocsRetrieved;
    private long elapsedTime = Long.MIN_VALUE;
    private final SynonymMap synonymMap;
    private final Analyzer analyzer;

    public SentenceSearcher(final Analyzer analyzer, final Similarity similarity, final String indexPath,
                            final String topicsFile, final int expectedTopics, final String runID, final String runPath,
                            final int maxDocsRetrieved, final SynonymMap synonymMap) {
        if (analyzer == null) {
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (indexPath == null) {
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }

        final Path indexDir = Paths.get(indexPath);
        if (!Files.isReadable(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be read.", indexDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(indexDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to search the index.",
                    indexDir.toAbsolutePath()));
        }

        try {
            reader = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index reader for directory %s: %s.",
                    indexDir.toAbsolutePath(), e.getMessage()), e);
        }

        searcher = new IndexSearcher(reader);
        searcher.setSimilarity(similarity);

        if (topicsFile == null) {
            throw new NullPointerException("Topics file cannot be null.");
        }

        if (topicsFile.isEmpty()) {
            throw new IllegalArgumentException("Topics file cannot be empty.");
        }

        this.synonymMap = synonymMap;

        try {
            topics = new ArrayList<>();
            XmlMapper topicsMapper = new XmlMapper();
            List<Map<String, String>> topicsList = topicsMapper.readValue(new File(topicsFile), new TypeReference<>() {
            });
            for (Map<String, String> topic : topicsList) {
                String queryID = topic.get(TOPIC_FIELDS.NUMBER);
                Map<String, String> values = new HashMap<>();
                values.put(TOPIC_FIELDS.TITLE, topic.get(TOPIC_FIELDS.TITLE));
                //values.put(TOPIC_FIELDS.DESCRIPTION, topic.get(TOPIC_FIELDS.DESCRIPTION));
                //values.put(TOPIC_FIELDS.NARRATIVE, topic.get(TOPIC_FIELDS.NARRATIVE));
                topics.add(new QualityQuery(queryID, values));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to process topic file %s: %s.", topicsFile, e.getMessage()), e);
        }

        if (expectedTopics <= 0) {
            throw new IllegalArgumentException(
                    "The expected number of topics to be searched cannot be less than or equal to zero.");
        }

        if (topics.size() != expectedTopics) {
            System.out.printf("Expected to search for %s topics; %s topics found instead.", expectedTopics,
                    topics.size());
        }

        //I have to make a query parse for each topic, so i will not initialize it here
        //qp = new QueryParser(ArgsFields.PREMISES_FIELDS.TEXT, analyzer);
        //qp = new QueryParser(ArgsFields.CONTEXT, analyzer);
        //qp = new QueryParser(ParameterConfig.LUCENE_DOC_FIELD_NAME, analyzer);
        //String[] indexed_fields = {ArgsFields.PREMISES_FIELDS.TEXT,ArgsFields.CONCLUSION, ArgsFields.SENTENCES_FIELDS.SENT_TEXT};
        //qp = new MultiFieldQueryParser(indexed_fields, analyzer);
        //qp = new QueryParser(null, analyzer);

        if (runID == null) {
            throw new NullPointerException("Run identifier cannot be null.");
        }

        if (runID.isEmpty()) {
            throw new IllegalArgumentException("Run identifier cannot be empty.");
        }

        this.runID = runID;


        if (runPath == null) {
            throw new NullPointerException("Run path cannot be null.");
        }

        if (runPath.isEmpty()) {
            throw new IllegalArgumentException("Run path cannot be empty.");
        }

        final Path runDir = Paths.get(runPath);
        if (!Files.isWritable(runDir)) {
            throw new IllegalArgumentException(
                    String.format("Run directory %s cannot be written.", runDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(runDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to write the run.",
                    runDir.toAbsolutePath()));
        }

        Path runFile = runDir.resolve(ParameterConfig.SENTENCES_RUN_FILE);
        Path touche_runFile = runDir.resolve(ParameterConfig.TOUCHE_RUN_FILE);
        try {
            run = new PrintWriter(Files.newBufferedWriter(runFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING,
                    StandardOpenOption.WRITE));
            touche_run = new PrintWriter(Files.newBufferedWriter(touche_runFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING,
                    StandardOpenOption.WRITE));
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to open run file %s: %s.", runFile.toAbsolutePath(), e.getMessage()), e);
        }

        if (maxDocsRetrieved <= 0) {
            throw new IllegalArgumentException(
                    "The maximum number of documents to be retrieved cannot be less than or equal to zero.");
        }

        this.maxDocsRetrieved = maxDocsRetrieved;
        this.analyzer = analyzer;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    public void search() throws IOException, ParseException {

        // the start time of the searching
        System.out.printf("%n#### Start searching ####%n");
        final long start = System.currentTimeMillis();

        try {
            for (int topic_index = 0; topic_index < topics.size(); topic_index++) {
                QualityQuery topic = topics.get(topic_index);
                String topicID = topic.getQueryID();
                int lines_per_topic = 0;
                String title = topic.getValue(TOPIC_FIELDS.TITLE);
                String original_title = topic.getValue(TOPIC_FIELDS.TITLE);

                //if i only have to decorate, then just
                if (ParameterConfig.ENABLE_QUERY_DECORATORS) {
                    for (String decorator : ParameterConfig.QUERY_DECORATORS) {
                        title += " " + decorator;
                    }
                }

                String[] stances = {"PRO", "CON"};
                for (String stance : stances) {
                    final Set<String> fields = new HashSet<>();
                    fields.add(ArgsFields.SENTENCES_FIELDS.SENT_ID);
                    //so this query will only search the portion of the sentence index with this name
                    String queryParserName = "query_" + topicID + "_" + stance;
                    fields.add(queryParserName);
                    sqp = new SimpleQueryParser(analyzer, queryParserName);
                    Query query = sqp.parse(QueryParserBase.escape(title));
                    Query original_query = sqp.parse(QueryParserBase.escape(original_title));
                    if (ParameterConfig.ENABLE_QUERY_BOOST) {
                        //pass threw analyzer and then build again (already passed)
                        //if i have decorators, i need to boost only the original terms, so i need to kwno where the decorator part is
                        BooleanQuery.Builder new_bqb = new BooleanQuery.Builder();
                        String parsed_query = query.toString(queryParserName);
                        String parsed_original_query = original_query.toString(queryParserName);
                        String[] words = parsed_original_query.split(" ");
                        for (String word : words) {
                            TermQuery tq1 = new TermQuery(new Term(queryParserName, word));
                            if (ParameterConfig.QUERY_BOOST != 1) {
                                BoostQuery boostQuery = new BoostQuery(tq1, ParameterConfig.QUERY_BOOST);
                                new_bqb.add(boostQuery, BooleanClause.Occur.SHOULD);
                            }
                            else {
                                new_bqb.add(tq1, BooleanClause.Occur.SHOULD);
                            }
                        }
                        if (ParameterConfig.ENABLE_QUERY_DECORATORS) {
                            //remove the original query to get only the decorator part
                            //all of this, just because i have to work with the query after the analyzer work
                            String parsed_decorators = parsed_query.replace(parsed_original_query,"");
                            String[] decorators_words = parsed_decorators.split(" ");
                            for (String decorator_word : decorators_words) {
                                //not adding boost here
                                if (decorator_word.length() > 0) {
                                    TermQuery tq1 = new TermQuery(new Term(queryParserName, decorator_word));
                                    new_bqb.add(tq1, BooleanClause.Occur.SHOULD);
                                }
                            }
                        }
                        query = new_bqb.build();
                    }
                    ScoreDoc[] scoreDocs = searcher.search(query, maxDocsRetrieved).scoreDocs;
                    //ScoreDoc[] scoreDocs = searcher.search(q, maxDocsRetrieved).scoreDocs;
                    boolean merge = false;
                    String prev_sentID = null;
                    float prev_score = 0;
                    int index_run = 1;
                    int index_touche_run = 1;
                    for (int i = 0; i < scoreDocs.length; i++) {
                        float score = scoreDocs[i].score;
                        Document document = reader.document(scoreDocs[i].doc, fields);
                        String sentID = document.get(ArgsFields.SENTENCES_FIELDS.SENT_ID);
                        String sentText = document.get(queryParserName);
                        //this means we have a duplicate. there's no other reason to have same score to the 6-th decimal place
                        if (score == prev_score) {
                            continue;
                        }
                        run.printf(Locale.ENGLISH, "%s\t%s\t%s  \t%d\t%.6f\t%s%n", topicID, stance, sentID, index_run, score, sentText);
                        index_run++;
                        if (merge) {
                            assert prev_sentID != null;
                            assert prev_score != 0;
                            float avg_score = (score + prev_score) / 2;
                            //give meaningfull names to different runs
                            touche_run.printf(Locale.ENGLISH, "%s %s %s,%s %d %.6f %s%n",
                                    topic.getQueryID(), stance, prev_sentID, sentID, index_touche_run, avg_score,
                                    ParameterConfig.TOUCHE_RUN_METHOD);
                            lines_per_topic++;
                            index_touche_run++;
                            merge = false;
                        } else {
                            merge = true;
                        }
                        prev_sentID = sentID;
                        prev_score = score;
                    }
                }
                run.flush();
                touche_run.flush();
                System.out.println("lines per topic " + topicID + " : " + lines_per_topic);
                assert lines_per_topic >= ParameterConfig.TOUCHE_MIN_LINES_PER_TOPIC;
                assert lines_per_topic <= ParameterConfig.TOUCHE_MAX_LINES_PER_TOPIC;
            }
        } finally {
            run.close();
            touche_run.close();
            reader.close();
        }

        elapsedTime = System.currentTimeMillis() - start;
        System.out.printf("%d topic(s) searched in %d seconds.", topics.size(), elapsedTime / 1000);
        System.out.printf("#### Searching complete ####%n");
    }

    public static void main(String[] args) throws Exception {

        String topics = ParameterConfig.TOPIC_PATH;

        final String sentenceIndexPath = ParameterConfig.SENTENCE_INDEX_PATH;
        final String runPath = ParameterConfig.EXPERIMENT_PATH;
        String runID = ParameterConfig.SENTENCES_RUN_ID;

        final int maxDocsRetrieved = ParameterConfig.MAX_DOCS_RETRIEVED_SECOND_RUN;

        final int expected_topics = ParameterConfig.EXPECTED_TOPICS;

        final Analyzer analyzer = ParameterConfig.analyzer;

        Similarity similarity = ParameterConfig.similarity;

        SentenceSearcher searcher = new SentenceSearcher(analyzer, similarity, sentenceIndexPath, topics, expected_topics, runID, runPath, maxDocsRetrieved, null);
        searcher.search();
    }
}