package it.unipd.dei.javacafe;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.en.KStemFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;

import java.io.IOException;

public class ParameterConfig {
    public static int MAX_DOCS_RETRIEVED_FIRST_RUN = 450;
    //query boost adds 4x weight to the original query terms
    public static boolean ENABLE_QUERY_BOOST = false;
    public static int QUERY_BOOST = 4;
    public static boolean ENABLE_QUERY_DECORATORS = false;
    public static boolean ENABLE_SYNONYMS = false;
    public static boolean ENABLE_REDUCED_TOPICS = true;
    public static boolean ENABLE_LMD_SIMILARITY = false;
    public static int MIN_WORDS_PER_SENTENCE = 20;
    public static int MAX_CHARS_PER_SENTENCE = 10000;


    public static boolean ENABLE_FIRST_PIPELINE = false;
    public static boolean ENABLE_SECOND_PIPELINE = true;

    static {
        if (ENABLE_FIRST_PIPELINE) {
            if (ENABLE_SECOND_PIPELINE) {
                String error_message = "Choose only one pipeline";
                throw new RuntimeException(error_message);
            }
            ENABLE_REDUCED_TOPICS = false;
            ENABLE_SYNONYMS = false;
            ENABLE_QUERY_BOOST = false;
            ENABLE_QUERY_DECORATORS = false;
        }
        if (ENABLE_SECOND_PIPELINE) {
            if (ENABLE_FIRST_PIPELINE) {
                String error_message = "Choose only one pipeline";
                throw new RuntimeException(error_message);
            }
            ENABLE_REDUCED_TOPICS = true;
            ENABLE_SYNONYMS = false;
            ENABLE_QUERY_BOOST = true;
            ENABLE_QUERY_DECORATORS = true;
        }
    }


    public static final String TEAM_NAME = "gamora";

    public static final String CORPUS_PATH = "C:\\Users\\Ralton\\Downloads\\ING\\Search Engines\\homework\\touche_task1\\args_processed.csv";
    public static final String REDUCED_TOPIC_PATH = System.getProperty("user.dir") + "\\topics\\topics_reduction.xml";
    public static final String TOPIC_PATH = ENABLE_REDUCED_TOPICS ? REDUCED_TOPIC_PATH : System.getProperty("user.dir") + "\\topics\\topics.xml";
    public static final String RUN_ID = ENABLE_SYNONYMS ? TEAM_NAME+"-synonyms" : TEAM_NAME;
    public static final String SENTENCES_RUN_ID = TEAM_NAME+"-sentences";
    public static final String SYNONYM_FILE_PATH = System.getProperty("user.dir") + "\\code1\\src\\main\\resources\\wn_s.pl";
    public static final String EXPERIMENT_PATH = System.getProperty("user.dir") + "/experiment/";
    public static final String INDEX_PATH = System.getProperty("user.dir") + "/experiment/index";
    public static final String SENTENCE_INDEX_PATH = System.getProperty("user.dir") + "/experiment/index-sentences";
    public static final String ENG_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ[";
    public static final String LUCENE_DOC_FIELD_NAME = "text";
    public static final int MAX_DOCS_RETRIEVED_SECOND_RUN = 700;
    public static final int EXPECTED_TOPICS = 50;
    public static final int EXPECTED_DOCS = 365408;
    public static final String DOCUMENTS_RUN_FILE = RUN_ID + ".documents.txt";
    public static final String SENTENCES_RUN_FILE = RUN_ID + ".sentences.txt";
    public static final String TOUCHE_RUN_FILE = RUN_ID + ".sentences.touche.txt";
    public static final String JSON_RUN_FILE = RUN_ID + ".json.txt";
    public static final String JSON_RUN_PATH = EXPERIMENT_PATH + "/" + RUN_ID + ".json.txt";
    public static String TOUCHE_RUN_METHOD = null;
    public static final int TOUCHE_MIN_LINES_PER_TOPIC = 100;
    public static final int TOUCHE_MAX_LINES_PER_TOPIC = 1000;


    static {
        //1-standard pipeline
        if (!ENABLE_REDUCED_TOPICS && !ENABLE_SYNONYMS && !ENABLE_QUERY_BOOST && !ENABLE_QUERY_DECORATORS) {
            TOUCHE_RUN_METHOD  = TEAM_NAME + "StandardDoubleIndex";
        }//2-heuristic pipeline
        if (ENABLE_REDUCED_TOPICS && !ENABLE_SYNONYMS && ENABLE_QUERY_BOOST && ENABLE_QUERY_DECORATORS) {
            TOUCHE_RUN_METHOD  = TEAM_NAME + "HeuristicsDoubleIndex";
        }//3- only query reduction pipeline
        //needed MAX_DOCS_FIRST_RUN = 450 to have at least 100 results per query
        if (ENABLE_REDUCED_TOPICS && !ENABLE_SYNONYMS && !ENABLE_QUERY_BOOST && !ENABLE_QUERY_DECORATORS) {
            TOUCHE_RUN_METHOD  = TEAM_NAME + "HeuristicsOnlyQueryReductionDoubleIndex";
        }//4- query reduction + query repetition pipeline
        //needed MAX_DOCS_FIRST_RUN = 450 to have at least 100 results per query
        if (ENABLE_REDUCED_TOPICS && !ENABLE_SYNONYMS && ENABLE_QUERY_BOOST && !ENABLE_QUERY_DECORATORS) {
            TOUCHE_RUN_METHOD  = TEAM_NAME + "reduced-boost";
        }//5- query reduction + query decoration
        if (ENABLE_REDUCED_TOPICS && !ENABLE_SYNONYMS && !ENABLE_QUERY_BOOST && ENABLE_QUERY_DECORATORS) {
            TOUCHE_RUN_METHOD  = TEAM_NAME + "reduced-decorators";
        }
        //Other pipeline (synonyms, reranking ...)
        if (TOUCHE_RUN_METHOD == null) {
            String error_message = "Specify the run method for this pipeline. This fields goes as the last column of the touche run format";
            throw new RuntimeException(error_message);

        }
    }

    public static Analyzer analyzer = null;
    public static Similarity similarity = ENABLE_LMD_SIMILARITY ? new LMDirichletSimilarity() : new BM25Similarity();

    //will this prove to be a good idea?
    public static String[] QUERY_DECORATORS = {
            "consequence",
            "conclusion",
            "reason",
            "information",
            "proof",
            "evidence",
            "argument",
            "fact",
            "show",
            "clear",
            "result",
            "mean",
            "enable",
            "follows",
            "therefore",
            //"research",
            //"literature",
            //"claim",
            //"because"
    };

    static {
        try {
            analyzer = CustomAnalyzer.builder()
                    .withTokenizer(StandardTokenizerFactory.class)
                    .addTokenFilter(LowerCaseFilterFactory.class)
                    .addTokenFilter(KStemFilterFactory.class)
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
