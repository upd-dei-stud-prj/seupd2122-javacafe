package it.unipd.dei.javacafe.utils;

public class Utils {

    //used just to stop debugger programmatically (like debugger keyword in js)
    //just put an intellij breakpoint next to the instruction
    public static void debugger() {
        int a = 1;
    }

    public static int getWordNumber(String s) {
        String trim = s.trim();
        if (trim.isEmpty())
            return 0;
        return trim.split("\\s+").length;
    }
}
