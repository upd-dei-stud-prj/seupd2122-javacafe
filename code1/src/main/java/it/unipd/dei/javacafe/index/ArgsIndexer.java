package it.unipd.dei.javacafe.index;

import it.unipd.dei.javacafe.ParameterConfig;
import it.unipd.dei.javacafe.parse.ArgsFields;
import it.unipd.dei.javacafe.parse.ArgsParsed;
import it.unipd.dei.javacafe.parse.ArgsParser;
import it.unipd.dei.javacafe.parse.DocumentParser;
import it.unipd.dei.javacafe.search.ArgsSearcher;
import it.unipd.dei.javacafe.search.SentenceSearcher;
import it.unipd.dei.javacafe.utils.Utils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class ArgsIndexer {

    private static final int MBYTE = 1024*1024;

    private final IndexWriter writer;
    private final Class<? extends DocumentParser> docParserClass;
    private final Path corpusPath;
    private final Charset cs;
    private final long expectedDocs;
    private final long start;
    private long filesCount;
    private long docsCount;
    private long bytesCount;

    public ArgsIndexer(final Analyzer analyzer, final Similarity similarity, final int ramBufferSizeMB,
                       final String indexPath, final String docsPath, final String charsetName,
                       final long expectedDocs, final Class<? extends DocumentParser> docParserClass) {
        if(docParserClass == null){
            throw new NullPointerException("Document parser class cannot be null.");
        }
        this.docParserClass = docParserClass;
        if (analyzer == null) {
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (ramBufferSizeMB <= 0) {
            throw new IllegalArgumentException("RAM buffer size cannot be less than or equal to zero.");
        }

        final IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
        iwc.setSimilarity(similarity);
        iwc.setRAMBufferSizeMB(ramBufferSizeMB);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        iwc.setCommitOnClose(true);
        iwc.setUseCompoundFile(true);

        if (indexPath == null) {
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }
        final Path indexDir = Paths.get(indexPath);
        if (Files.notExists(indexDir)) {
            try {
                //replacing .createDirectory to create nested directories
                Files.createDirectories(indexDir);
            } catch (Exception e) {
                throw new IllegalArgumentException(
                        String.format("Unable to create directory %s: %s.", indexDir.toAbsolutePath().toString(),
                                e.getMessage()), e);
            }
        }

        if (docsPath == null) {
            throw new NullPointerException("Documents path cannot be null.");
        }

        if (docsPath.isEmpty()) {
            throw new IllegalArgumentException("Documents path cannot be empty.");
        }

        final Path docsDir = Paths.get(docsPath);
        if (!Files.isReadable(docsDir)) {
            throw new IllegalArgumentException(
                    String.format("Documents path %s cannot be read.", docsDir.toAbsolutePath().toString()));
        }

        this.corpusPath = docsDir;

        if (charsetName == null) {
            throw new NullPointerException("Charset name cannot be null.");
        }

        if (charsetName.isEmpty()) {
            throw new IllegalArgumentException("Charset name cannot be empty.");
        }

        try {
            cs = Charset.forName(charsetName);
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("Unable to create the charset %s: %s.", charsetName, e.getMessage()), e);
        }

        if (expectedDocs <= 0) {
            throw new IllegalArgumentException(
                    "The expected number of documents to be indexed cannot be less than or equal to zero.");
        }
        this.expectedDocs = expectedDocs;

        this.docsCount = 0;

        this.bytesCount = 0;

        this.filesCount = 0;

        try {
            writer = new IndexWriter(FSDirectory.open(indexDir), iwc);
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index writer in directory %s: %s.",
                    indexDir.toAbsolutePath(), e.getMessage()), e);
        }

        this.start = System.currentTimeMillis();
    }

    public void index() throws IOException{
        System.out.printf("%n#### Start indexing ####%n");

        //actually ArgsParser
        DocumentParser docParser = DocumentParser.create(docParserClass, Files.newBufferedReader(corpusPath,cs));

        bytesCount += Files.size(corpusPath);
        filesCount += 1;

        System.out.printf("Indexing %d expected documents and %d Mbytes\n",expectedDocs, bytesCount / MBYTE);

        for (ArgsParsed doc : docParser) {
            if (doc == null) {
                continue;
            }
            Document luceneDoc = new Document();
            String id = doc.getId();
            //not adding individual fields in luceneDoc, just creating a big string with
            //title and sentences(with some criteria)
            StringBuilder stringBuilder = new StringBuilder();

            luceneDoc.add(new StringField(ArgsFields.ID, doc.getId(), Field.Store.YES));
            String title = doc.getContextTitle();
            stringBuilder.append(title);
            stringBuilder.append(doc.getConclusion());
            //String contextSourceText = doc.getContextSourceText();
            //luceneDoc.add(new TextField(ArgsFields.CONTEXT_FIELDS.CONTEXT_TITLE,title,Field.Store.NO));
            //luceneDoc.add(new TextField(ArgsFields.CONCLUSION,doc.getConclusion(),Field.Store.NO));
            //we already get the premises from sentences. Even better, they are already parsed and splitted
            //luceneDoc.add(new TextField(ArgsFields.PREMISES_FIELDS.TEXT,doc.getPremisesText(),Field.Store.NO));
            //luceneDoc.add(new StringField(ArgsFields.PREMISES_FIELDS.STANCE,doc.getPremisesStance(),Field.Store.YES));


            List<Map<String, ?>> sentencs = doc.getSentences();
            int sentence_number = sentencs.size();
            for(int i = 0; i < sentence_number; i++) {
                Map<String,?> sentence = sentencs.get(i);
                String sent_id =  (String) sentence.get(ArgsFields.SENTENCES_FIELDS.SENT_ID);
                String sent_text = (String) sentence.get(ArgsFields.SENTENCES_FIELDS.SENT_TEXT);
                String first_letter = sent_text.substring(0,1);
                int sent_words = Utils.getWordNumber(sent_text);
                if (ParameterConfig.ENG_UPPER.contains(first_letter)
                        && sent_words > ParameterConfig.MIN_WORDS_PER_SENTENCE
                        && sent_text.length() < ParameterConfig.MAX_CHARS_PER_SENTENCE) {
                    //luceneDoc.add(new StringField(ArgsFields.SENTENCES_FIELDS.SENT_ID, sent_id, Field.Store.NO));
                    //luceneDoc.add(new TextField(ArgsFields.SENTENCES_FIELDS.SENT_TEXT, sent_text, Field.Store.NO));
                    if (!sent_id.contains("CONC")) {
                        stringBuilder.append(sent_text);
                    }
                }
                else {
                    /* OLD CODE
                    //ok, so this is slow, but not too slow. I mean i have to remove something like max 10 strings
                    //so it will be at max 10 times slower. I guess it will be like 5 times slower in average.
                    //a normal indexing will take like 5 minutes, so 25 minutes it's not so big deal
                    //so I don't concern with performance as of now, but I probably will if this turns out to be a bottleneck
                    //edit : it was complete bullshit, sentences are part of the premise and they don't really remove so mutch useless frases from the contextText
                    //contextSourceText = contextSourceText.replace(sent_text, "");
                     */
                }
            }

            luceneDoc.add(new TextField(ParameterConfig.LUCENE_DOC_FIELD_NAME, stringBuilder.toString(), Field.Store.NO));

            writer.addDocument(luceneDoc);
            docsCount++;
            if (docsCount % 500 == 0) {
                System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n",
                        docsCount, filesCount, bytesCount / MBYTE,
                        (System.currentTimeMillis() - start) / 1000);
                //remove this break. Inserted just to test and break quickly
                //break;
            }
        }
        writer.commit();
        writer.close();
        if (docsCount != expectedDocs) {
            System.out.printf("Expected to index %d documents; %d indexed instead.%n", expectedDocs, docsCount);
        }

        System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n", docsCount, filesCount,
                bytesCount / MBYTE, (System.currentTimeMillis() - start) / 1000);

        System.out.printf("#### Indexing complete ####%n");
    }

    //document indexing
    public static void main(String[] args) throws Exception {

        final int ramBuffer = 512;
        String docsPath = ParameterConfig.CORPUS_PATH;
        final String indexPath = ParameterConfig.INDEX_PATH;
        final int expectedDocs = ParameterConfig.EXPECTED_DOCS;
        final String charsetName = "UTF-8";

        final Analyzer analyzer = ParameterConfig.analyzer;

        Similarity similarity = ParameterConfig.similarity;

        ArgsIndexer i = new ArgsIndexer(analyzer, similarity, ramBuffer, indexPath, docsPath,
                charsetName, expectedDocs, ArgsParser.class);

        i.index();
        ArgsSearcher.main(null);
        SentenceIndexer.main(null);
        SentenceSearcher.main(null);
    }
}
