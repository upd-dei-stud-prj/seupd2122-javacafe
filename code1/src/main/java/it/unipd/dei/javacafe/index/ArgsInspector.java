package it.unipd.dei.javacafe.index;

import it.unipd.dei.javacafe.ParameterConfig;
import it.unipd.dei.javacafe.parse.ArgsFields;
import it.unipd.dei.javacafe.parse.ArgsParsed;
import it.unipd.dei.javacafe.parse.ArgsParser;
import it.unipd.dei.javacafe.parse.DocumentParser;
import it.unipd.dei.javacafe.utils.Utils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

/**
 * Class to be used just for debugging purpuses
 * Will loop through the documents and will stop on breakpoints to inspect document fields
 */
public class ArgsInspector {

    private static final int MBYTE = 1024*1024;

    private final IndexWriter writer;
    private final Class<? extends DocumentParser> docParserClass;
    private final Path corpusPath;
    private final Charset cs;
    private final long expectedDocs;
    private final long start;
    private long filesCount;
    private long docsCount;
    private long bytesCount;

    public ArgsInspector(final Analyzer analyzer, final Similarity similarity, final int ramBufferSizeMB,
                         final String indexPath, final String docsPath, final String charsetName,
                         final long expectedDocs, final Class<? extends DocumentParser> docParserClass) {
        if(docParserClass == null){
            throw new NullPointerException("Document parser class cannot be null.");
        }
        this.docParserClass = docParserClass;
        if (analyzer == null) {
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (ramBufferSizeMB <= 0) {
            throw new IllegalArgumentException("RAM buffer size cannot be less than or equal to zero.");
        }

        final IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
        iwc.setSimilarity(similarity);
        iwc.setRAMBufferSizeMB(ramBufferSizeMB);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        iwc.setCommitOnClose(true);
        iwc.setUseCompoundFile(true);

        if (indexPath == null) {
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }
        final Path indexDir = Paths.get(indexPath);
        if (Files.notExists(indexDir)) {
            try {
                //replacing .createDirectory to create nested directories
                Files.createDirectories(indexDir);
            } catch (Exception e) {
                throw new IllegalArgumentException(
                        String.format("Unable to create directory %s: %s.", indexDir.toAbsolutePath().toString(),
                                e.getMessage()), e);
            }
        }

        if (docsPath == null) {
            throw new NullPointerException("Documents path cannot be null.");
        }

        if (docsPath.isEmpty()) {
            throw new IllegalArgumentException("Documents path cannot be empty.");
        }

        final Path docsDir = Paths.get(docsPath);
        if (!Files.isReadable(docsDir)) {
            throw new IllegalArgumentException(
                    String.format("Documents path %s cannot be read.", docsDir.toAbsolutePath().toString()));
        }

        this.corpusPath = docsDir;

        if (charsetName == null) {
            throw new NullPointerException("Charset name cannot be null.");
        }

        if (charsetName.isEmpty()) {
            throw new IllegalArgumentException("Charset name cannot be empty.");
        }

        try {
            cs = Charset.forName(charsetName);
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("Unable to create the charset %s: %s.", charsetName, e.getMessage()), e);
        }

        if (expectedDocs <= 0) {
            throw new IllegalArgumentException(
                    "The expected number of documents to be indexed cannot be less than or equal to zero.");
        }
        this.expectedDocs = expectedDocs;

        this.docsCount = 0;

        this.bytesCount = 0;

        this.filesCount = 0;

        try {
            writer = new IndexWriter(FSDirectory.open(indexDir), iwc);
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index writer in directory %s: %s.",
                    indexDir.toAbsolutePath(), e.getMessage()), e);
        }

        this.start = System.currentTimeMillis();
    }

    public void index() throws IOException{
        DocumentParser docParser = DocumentParser.create(docParserClass, Files.newBufferedReader(corpusPath,cs));

        for (ArgsParsed doc : docParser) {
            if (doc == null) {
                continue;
            }
            String id = doc.getId();
            if (id.equals("Sc065954f-A24a16870")) {
                //Utils.debugger();
            }

            String conclusion = doc.getConclusion();

            List<Map<String, ?>> sentences = doc.getSentences();
            for (int i = 0; i < sentences.size(); i++) {
                Map<String, ?> sentence = sentences.get(i);
                String sent_id = (String) sentence.get(ArgsFields.SENTENCES_FIELDS.SENT_ID);
                String sent_text = (String) sentence.get(ArgsFields.SENTENCES_FIELDS.SENT_TEXT);
                if (sent_id.equals("S1b03f390-Aa73ba80f__PREMISE__50")) {
                    //Utils.debugger();
                }
                if (sent_text.length() > 10000) {
                    //Utils.debugger();
                }
                if (i == sentences.size() - 1) {
                    if (!sent_text.equals(conclusion)) {
                        //Utils.debugger();
                    }
                }
                if (sent_text.startsWith("This is analogous stating")) {
                    //Utils.debugger();
                }
            }
        }
    }

    //document indexing
    public static void main(String[] args) throws Exception {

        final int ramBuffer = 512;
        String docsPath = ParameterConfig.CORPUS_PATH;
        //relative path
        final String indexPath = ParameterConfig.INDEX_PATH;

        final int expectedDocs = ParameterConfig.EXPECTED_DOCS;
        final String charsetName = "UTF-8";

        final Analyzer analyzer = ParameterConfig.analyzer;

        Similarity similarity = ParameterConfig.similarity;

        ArgsInspector i = new ArgsInspector(analyzer, similarity, ramBuffer, indexPath, docsPath,
                charsetName, expectedDocs, ArgsParser.class);


        i.index();
    }
}
