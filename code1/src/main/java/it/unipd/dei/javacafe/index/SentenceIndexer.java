package it.unipd.dei.javacafe.index;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.unipd.dei.javacafe.ParameterConfig;
import it.unipd.dei.javacafe.parse.ArgsFields;
import it.unipd.dei.javacafe.parse.ArgsParsed;
import it.unipd.dei.javacafe.parse.ArgsParser;
import it.unipd.dei.javacafe.parse.DocumentParser;
import it.unipd.dei.javacafe.utils.Utils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SentenceIndexer {

    private static final int MBYTE = 1024*1024;

    private final IndexWriter writer;
    private final Class<? extends DocumentParser> docParserClass;
    private final Path corpusPath;
    private final Charset cs;
    private final long expectedDocs;
    private final long start;
    private long filesCount;
    private long docsCount;
    private long bytesCount;

    public SentenceIndexer(final Analyzer analyzer, final Similarity similarity, final int ramBufferSizeMB,
                           final String indexPath, final String docsPath, final String charsetName,
                           final long expectedDocs, final Class<? extends DocumentParser> docParserClass) {
        if(docParserClass == null){
            throw new NullPointerException("Document parser class cannot be null.");
        }
        this.docParserClass = docParserClass;
        if (analyzer == null) {
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (ramBufferSizeMB <= 0) {
            throw new IllegalArgumentException("RAM buffer size cannot be less than or equal to zero.");
        }

        final IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
        iwc.setSimilarity(similarity);
        iwc.setRAMBufferSizeMB(ramBufferSizeMB);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        iwc.setCommitOnClose(true);
        iwc.setUseCompoundFile(true);

        if (indexPath == null) {
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }
        final Path indexDir = Paths.get(indexPath);
        if (Files.notExists(indexDir)) {
            try {
                //replacing .createDirectory to create nested directories
                Files.createDirectories(indexDir);
            } catch (Exception e) {
                throw new IllegalArgumentException(
                        String.format("Unable to create directory %s: %s.", indexDir.toAbsolutePath().toString(),
                                e.getMessage()), e);
            }
        }

        if (docsPath == null) {
            throw new NullPointerException("Documents path cannot be null.");
        }

        if (docsPath.isEmpty()) {
            throw new IllegalArgumentException("Documents path cannot be empty.");
        }

        final Path docsDir = Paths.get(docsPath);
        if (!Files.isReadable(docsDir)) {
            throw new IllegalArgumentException(
                    String.format("Documents path %s cannot be read.", docsDir.toAbsolutePath().toString()));
        }

        this.corpusPath = docsDir;

        if (charsetName == null) {
            throw new NullPointerException("Charset name cannot be null.");
        }

        if (charsetName.isEmpty()) {
            throw new IllegalArgumentException("Charset name cannot be empty.");
        }

        try {
            cs = Charset.forName(charsetName);
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("Unable to create the charset %s: %s.", charsetName, e.getMessage()), e);
        }

        if (expectedDocs <= 0) {
            throw new IllegalArgumentException(
                    "The expected number of documents to be indexed cannot be less than or equal to zero.");
        }
        this.expectedDocs = expectedDocs;

        this.docsCount = 0;

        this.bytesCount = 0;

        this.filesCount = 0;

        try {
            writer = new IndexWriter(FSDirectory.open(indexDir), iwc);
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index writer in directory %s: %s.",
                    indexDir.toAbsolutePath(), e.getMessage()), e);
        }

        this.start = System.currentTimeMillis();
    }

    public void index() throws IOException {
        System.out.printf("%n#### Start sentence indexing ####%n");

        String jsonRunPath = ParameterConfig.JSON_RUN_PATH;
        Map<String, Set<Integer>> first_run = new ObjectMapper().readValue(new File(jsonRunPath), new TypeReference<>(){});

        //actually ArgsParser
        DocumentParser docParser = DocumentParser.create(docParserClass, Files.newBufferedReader(corpusPath,cs));

        bytesCount += Files.size(corpusPath);
        filesCount += 1;

        System.out.printf("Indexing %d expected documents and %d Mbytes\n",expectedDocs, bytesCount / MBYTE);

        for (ArgsParsed doc : docParser) {
            if (doc == null) {
                continue;
            }
            String doc_id = doc.getId();
            String stance = doc.getPremisesStance();
            //luceneDoc.add(new StringField(ArgsFields.PREMISES_FIELDS.STANCE,stance,Field.Store.YES));

            Set<Integer> topics_involved = first_run.get(doc_id);
            if (topics_involved == null) {
                docsCount++;
                if (docsCount % 500 == 0) {
                    System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n",
                            docsCount, filesCount, bytesCount / MBYTE,
                            (System.currentTimeMillis() - start) / 1000);
                    //remove this break. Inserted just to test and break quickly
                    //break;
                }
                continue;
            }

            for (Integer topic_number : topics_involved) {
                //will use a custom filed name, so to search specifically on these sentences when I'll have the right query
                String textFieldName = "query_" + topic_number + "_" + stance;
                List<Map<String, ?>> sentences = doc.getSentences();
                int sentence_number = sentences.size();
                for (int i = 0; i < sentence_number; i++) {
                    Document luceneDoc = new Document();
                    Map<String, ?> sentence = sentences.get(i);
                    String sent_id = (String) sentence.get(ArgsFields.SENTENCES_FIELDS.SENT_ID);
                    String sent_text = (String) sentence.get(ArgsFields.SENTENCES_FIELDS.SENT_TEXT);
                    String first_letter = sent_text.substring(0, 1);
                    int sent_words = Utils.getWordNumber(sent_text);
                    //so the sentence in order to be indexed has to start with an uppercase english letter
                    //has to have more than 20 words (doesn't have to be too short)
                    //here i remove the mandatory conclusion to be indexed, it can be non-informative if too short
                    //remove huge sentences
                    if (ParameterConfig.ENG_UPPER.contains(first_letter)
                            && sent_words > ParameterConfig.MIN_WORDS_PER_SENTENCE
                            && sent_text.length() < ParameterConfig.MAX_CHARS_PER_SENTENCE) {
                        luceneDoc.add(new StringField(ArgsFields.SENTENCES_FIELDS.SENT_ID, sent_id, Field.Store.YES));
                        luceneDoc.add(new TextField(textFieldName, sent_text, Field.Store.YES));
                        writer.addDocument(luceneDoc);
                    }
                }
            }

            docsCount++;
            if (docsCount % 500 == 0) {
                System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n",
                        docsCount, filesCount, bytesCount / MBYTE,
                        (System.currentTimeMillis() - start) / 1000);
                //remove this break. Inserted just to test and break quickly
                //break;
            }
        }
        writer.commit();
        writer.close();
        if (docsCount != expectedDocs) {
            System.out.printf("Expected to index %d documents; %d indexed instead.%n", expectedDocs, docsCount);
        }

        System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n", docsCount, filesCount,
                bytesCount / MBYTE, (System.currentTimeMillis() - start) / 1000);

        System.out.printf("#### Indexing complete ####%n");
    }

    //document indexing
    public static void main(String[] args) throws Exception {

        final int ramBuffer = 512;
        String docsPath = ParameterConfig.CORPUS_PATH;
        final String sentenceIndexPath = ParameterConfig.SENTENCE_INDEX_PATH;

        final int expectedDocs = ParameterConfig.EXPECTED_DOCS;
        final String charsetName = "UTF-8";

        final Analyzer analyzer = ParameterConfig.analyzer;

        Similarity similarity = ParameterConfig.similarity;

        SentenceIndexer i = new SentenceIndexer(analyzer, similarity, ramBuffer, sentenceIndexPath, docsPath,
                charsetName, expectedDocs, ArgsParser.class);

        i.index();
    }
}
