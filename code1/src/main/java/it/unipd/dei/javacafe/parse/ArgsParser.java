package it.unipd.dei.javacafe.parse;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ArgsParser extends DocumentParser {

    private ArgsParsed document = null;

    private final JsonMapper jsonMapper;
    private final MappingIterator<Map<String, String>> csvIterator;

    public ArgsParser(final Reader in) throws IOException {
        super(new BufferedReader(in));
        //There are two problems with the json string.
        //First, it's not correct json, because key values are wrapped in single quotes instead of double quotes
        //Second, some characters are not correctly escaped by double \\, so we need to allow for single backslash escaping
        jsonMapper = JsonMapper.builder()
                .configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true)
                .enable(JsonReadFeature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER)
                .build();
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader(); // use first row as header; otherwise defaults are fine
        csvIterator = csvMapper.readerFor(Map.class)
                .with(schema)
                .readValues(in);
    }

    @Override
    public boolean hasNext() {
        boolean hasNext = csvIterator.hasNext();
        if (hasNext) {
            Map<String, String> rowAsMap = csvIterator.next();
            try {
                String id = rowAsMap.get(ArgsFields.ID);
                String conclusion = rowAsMap.get(ArgsFields.CONCLUSION);
                List<Map<String, ?>> premises = jsonMapper.readValue(rowAsMap.get(ArgsFields.PREMISES), new TypeReference<>() {});
                Map<String, ?> context = jsonMapper.readValue(rowAsMap.get(ArgsFields.CONTEXT), new TypeReference<>() {});
                ArrayList<Map<String, ?>> sentences = jsonMapper.readValue(rowAsMap.get(ArgsFields.SENTENCES), new TypeReference<>() {});
                document = new ArgsParsed(id, conclusion, premises, context, sentences);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return hasNext;
    }

    @Override
    protected ArgsParsed parse() {
        return document;
    }
}