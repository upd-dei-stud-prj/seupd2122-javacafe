# Search Engines (SE) - Gamora Team

This repository is a template repository for the homeworks to be developed in the [Search Engines](https://iiia.dei.unipd.it/education/search-engines/) course.

The homeworks are carried out by groups of students and consists in participating to one of the labs organized yearly by [CLEF](https://www.clef-initiative.eu/) (Conference and Labs of the Evaluation Forum).

*Search Engines* is a course of the

* [Master Degree in Computer Engineering](https://degrees.dei.unipd.it/master-degrees/computer-engineering/) of the  [Department of Information Engineering](https://www.dei.unipd.it/en/), [University of Padua](https://www.unipd.it/en/), Italy.
* [Master Degree in Data Science](https://datascience.math.unipd.it/) of the  [Department of Mathematics "Tullio Levi-Civita"](https://www.math.unipd.it/en/), [University of Padua](https://www.unipd.it/en/), Italy.

*Search Engines* is part of the teaching activities of the [Intelligent Interactive Information Access (IIIA) Hub](http://iiia.dei.unipd.it/).

## Touchè @ CLEF2022 - Task 1: Argument Retrieval for Controversial Questions

The goal of Task 1 is to support users who search for arguments to be used in conversations (e.g., getting an overview of pros and cons or just looking for arguments in line with a user's stance). The requirement is to retrieve a pair of sentences from a collection of arguments. Each sentence in this pair must be argumentative (e.g., a claim, a supporting premise, or a conclusion). Also, to encourage diversity of the retrieved results, sentences in this pair may come from two different arguments.

## Group members

| Surname       | Name          | Contacts            |
| ------------- | ------------- | ------------- |
| Benetti		| Alessandro	| alessandro.benetti.1@studenti.unipd.it		|
| De Togni		| Michele		| michele.detogni@studenti.unipd.it		|
| Foti			| Giovanni		| giovanni.foti@studenti.unipd.it       |
| Lacini		| Ralton		| ralton.lacini@studenti.unipd.it		|
| Matteazzi 	| Andrea		| andrea.matteazzi.2@studenti.unipd.it		|
| Sgarbossa		| Enrico		| enrico.sgarbossa.1@studenti.unipd.it		|

### Organisation of the repository ###

The repository is organised as follows:

* `code1`: this folder contains the source code of the developed solution 1.
* `code2`: this folder contains the source code of the developed solution 2.
* `experiment`: this folder contains the experimental runs.
* `homework-1`: this folder contains the report describing the techniques applied and insights gained.
* `homework-2`: this folder contains the final paper submitted to CLEF.
* `matlab`: this folder contains the matlab code used for the statistical analysis.
* `results`: this folder contains the performance scores of the runs.
* `runs`: this folder contains the runs produced by the developed solutions 1 and 2.
* `topics`: this folder contains the original topics and the reduced topics.
* `slides`: this folder contains the slides used for presenting the conducted project.

### License ###

All the contents of this repository are shared using the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

![CC logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

