package it.unipd.dei.javacafe.search;

import com.fasterxml.jackson.core.type.TypeReference;
import it.unipd.dei.javacafe.parse.ArgsFields;
import it.unipd.dei.javacafe.parse.ParsedDocument;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.StopFilterFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.en.KStemFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.benchmark.quality.QualityQuery;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class ArgsSearcher {
    private final QueryParser conclusionQueryParser;

    private static final class TOPIC_FIELDS{
        public static final String NUMBER = "number";
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
        public static final String NARRATIVE = "narrative";
    }
    static class Topic {
        /**
         * The topic number
         */
        private String number;

        /**
         * The title of the topic
         */
        private String title;

        /**
         * The description of the topic
         */
        private String description;

        /**
         * The narrative of the topic
         */
        private String narrative;
    }
    private final String runID;
    private final PrintWriter run;
    private final IndexReader reader;
    private final IndexSearcher searcher;
    private final List<QualityQuery> topics;
    private final QueryParser premisesTextQueryParser;
    private final QueryParser sentencesTextQueryParser;
    private final MultiFieldQueryParser multiFieldQueryParser;
    private final int maxDocsRetrieved;
    private long elapsedTime = Long.MIN_VALUE;
    public ArgsSearcher(final Analyzer analyzer, final Similarity similarity, final String indexPath,
                        final String topicsFile, final int expectedTopics, final String runID, final String runPath,
                        final int maxDocsRetrieved){
        if (analyzer == null) {
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (indexPath == null) {
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }

        final Path indexDir = Paths.get(indexPath);
        if (!Files.isReadable(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be read.", indexDir.toAbsolutePath().toString()));
        }

        if (!Files.isDirectory(indexDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to search the index.",
                    indexDir.toAbsolutePath().toString()));
        }

        try {
            reader = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index reader for directory %s: %s.",
                    indexDir.toAbsolutePath().toString(), e.getMessage()), e);
        }

        searcher = new IndexSearcher(reader);
        searcher.setSimilarity(similarity);

        if (topicsFile == null) {
            throw new NullPointerException("Topics file cannot be null.");
        }

        if (topicsFile.isEmpty()) {
            throw new IllegalArgumentException("Topics file cannot be empty.");
        }

        try {
            BufferedReader in = Files.newBufferedReader(Paths.get(topicsFile), StandardCharsets.UTF_8);
            topics = new ArrayList<>();
            XmlMapper topicsMapper = new XmlMapper();
            List<Map<String,String>> topicsList = topicsMapper.readValue(in, new TypeReference<List<Map<String,String>>>(){});
            topicsList.iterator().forEachRemaining((Map<String,String> e) ->
            {
                String queryID = e.get(TOPIC_FIELDS.NUMBER);
                Map<String,String> values = new HashMap<>();
                values.put(TOPIC_FIELDS.TITLE, e.get(TOPIC_FIELDS.TITLE));
                values.put(TOPIC_FIELDS.DESCRIPTION, e.get(TOPIC_FIELDS.DESCRIPTION));
                values.put(TOPIC_FIELDS.NARRATIVE, e.get(TOPIC_FIELDS.NARRATIVE));
                topics.add(new QualityQuery(queryID, values));
            });
            in.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to process topic file %s: %s.", topicsFile, e.getMessage()), e);
        }

        if (expectedTopics <= 0) {
            throw new IllegalArgumentException(
                    "The expected number of topics to be searched cannot be less than or equal to zero.");
        }

        if (topics.size() != expectedTopics) {
            System.out.printf("Expected to search for %s topics; %s topics found instead.", expectedTopics,
                    topics.size());
        }

        premisesTextQueryParser = new QueryParser(ArgsFields.PREMISES.TEXT, analyzer);
        sentencesTextQueryParser = new QueryParser(ArgsFields.SENTENCES.SENT_TEXT, analyzer);
        this.conclusionQueryParser = new QueryParser(ParsedDocument.FIELDS.CONCLUSION, analyzer);
        // TODO add discuss title and or topic
        String[] fields = new String[]{
                ArgsFields.CONTEXT.TOPIC,
                ArgsFields.CONTEXT.DISCUSSION_TITLE,
                ParsedDocument.FIELDS.CONCLUSION,
                ParsedDocument.FIELDS.PREMISES
        };
        Map<String,Float> boosts = new HashMap<>();
        boosts.put(ArgsFields.CONTEXT.TOPIC,0.5F);
        boosts.put(ArgsFields.CONTEXT.DISCUSSION_TITLE,0.5F);
        boosts.put(ParsedDocument.FIELDS.CONCLUSION,0.25F);
        boosts.put(ParsedDocument.FIELDS.PREMISES,1F);
        multiFieldQueryParser = new MultiFieldQueryParser(fields,analyzer,boosts);
        // Set operator for multi field query parser. Default: AND
        multiFieldQueryParser.setDefaultOperator(QueryParser.Operator.OR);
        // Set if the query parser should generate synonyms for phrase queries. Default: false
        multiFieldQueryParser.setAutoGenerateMultiTermSynonymsPhraseQuery(true);
        // Set if the query parser should split query text on whitespaces prior to analysis. Default: false
        multiFieldQueryParser.setSplitOnWhitespace(true);

        if (runID == null) {
            throw new NullPointerException("Run identifier cannot be null.");
        }

        if (runID.isEmpty()) {
            throw new IllegalArgumentException("Run identifier cannot be empty.");
        }

        this.runID = runID;


        if (runPath == null) {
            throw new NullPointerException("Run path cannot be null.");
        }

        if (runPath.isEmpty()) {
            throw new IllegalArgumentException("Run path cannot be empty.");
        }

        final Path runDir = Paths.get(runPath);
        if (!Files.isWritable(runDir)) {
            throw new IllegalArgumentException(
                    String.format("Run directory %s cannot be written.", runDir.toAbsolutePath().toString()));
        }

        if (!Files.isDirectory(runDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to write the run.",
                    runDir.toAbsolutePath().toString()));
        }

        Path runFile = runDir.resolve(runID + ".txt");
        try {
            run = new PrintWriter(Files.newBufferedWriter(runFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING,
                    StandardOpenOption.WRITE));
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to open run file %s: %s.", runFile.toAbsolutePath(), e.getMessage()), e);
        }

        if (maxDocsRetrieved <= 0) {
            throw new IllegalArgumentException(
                    "The maximum number of documents to be retrieved cannot be less than or equal to zero.");
        }

        this.maxDocsRetrieved = maxDocsRetrieved;
    }
    public long getElapsedTime() {
        return elapsedTime;
    }

    public void search() throws IOException, ParseException {

        System.out.printf("%n#### Start searching ####%n");

        // the start time of the searching
        final long start = System.currentTimeMillis();

        final Set<String> fields = new HashSet<>();
        fields.add(ParsedDocument.FIELDS.ID);
        fields.add(ArgsFields.SENTENCES.SENT_ID);
        fields.add(ArgsFields.SENTENCES.SENT_TEXT);
        fields.add(ArgsFields.PREMISES.STANCE);
        fields.add(ParsedDocument.FIELDS.PREMISES);

        BooleanQuery.Builder boolQueryBuilder = null;
        Query query = null;
        TopDocs docs = null;
        ScoreDoc[] scoreDocsArray = null;
        String docID = null;

        boolean print_sentences = false;
        try {
            for (QualityQuery t : topics) {

                System.out.printf("Searching for topic %s.%n", t.getQueryID());

                String title = t.getValue(TOPIC_FIELDS.TITLE);
                boolQueryBuilder = new BooleanQuery.Builder();

                boolQueryBuilder.add(premisesTextQueryParser.parse(QueryParserBase.escape(title)), BooleanClause.Occur.SHOULD);
//                bq.add(qp.parse(QueryParserBase.escape(t.getValue(TOPIC_FIELDS.DESCRIPTION))), BooleanClause.Occur.SHOULD);

//                boolQueryBuilder.add(multiFieldQueryParser.createBooleanQuery(ArgsFields.CONTEXT.TOPIC,title),BooleanClause.Occur.SHOULD);
//                boolQueryBuilder.add(multiFieldQueryParser.createBooleanQuery(ArgsFields.CONTEXT.DISCUSSION_TITLE,title),BooleanClause.Occur.SHOULD);
//                boolQueryBuilder.add(multiFieldQueryParser.createBooleanQuery(ParsedDocument.FIELDS.CONCLUSION,title),BooleanClause.Occur.SHOULD);
//                boolQueryBuilder.add(multiFieldQueryParser.createBooleanQuery(ParsedDocument.FIELDS.PREMISES,title),BooleanClause.Occur.SHOULD);
//                boolQueryBuilder.add(multiFieldQueryParser.createBooleanQuery(ArgsFields.CONTEXT.TOPIC,title),BooleanClause.Occur.SHOULD);
//                boolQueryBuilder.add(multiFieldQueryParser.createBooleanQuery(ArgsFields.CONTEXT.DISCUSSION_TITLE,title),BooleanClause.Occur.SHOULD);
//                boolQueryBuilder.add(multiFieldQueryParser.createPhraseQuery(ParsedDocument.FIELDS.CONCLUSION,title),BooleanClause.Occur.SHOULD);
//                boolQueryBuilder.add(multiFieldQueryParser.createPhraseQuery(ParsedDocument.FIELDS.PREMISES,title),BooleanClause.Occur.SHOULD);
//                boolQueryBuilder.add(sentencesTextQueryParser.parse(QueryParserBase.escape(title)),BooleanClause.Occur.SHOULD);
//                boolQueryBuilder.add(conclusionQueryParser.parse(QueryParserBase.escape(title)),BooleanClause.Occur.SHOULD);
                query = boolQueryBuilder.build();


                docs = searcher.search(query, maxDocsRetrieved);

                scoreDocsArray = docs.scoreDocs;

                for (int rank = 1, n = scoreDocsArray.length; rank < n+1; rank++) {
                    int i = rank-1;
                    String stance = "Q0";
                    Document document = reader.document(scoreDocsArray[i].doc, fields);
                    docID = document.get(ParsedDocument.FIELDS.ID);
                    String[] sent_id = document.getValues(ArgsFields.SENTENCES.SENT_ID);
                    stance = document.get(ArgsFields.PREMISES.STANCE);
//                    run.printf(Locale.ENGLISH, "%s\t%s\t%s\t%d\t%.6f\t%s%n", t.getQueryID(),stance, docID, i, sd[i].score, runID);
                    //TODO change output to desired touché format
                    if(print_sentences){
                        if (sent_id.length > 1) {
                            run.printf(Locale.ENGLISH, "%s\t%s\t%s\t%s\t%d\t%.6f\t%s%n", t.getQueryID(), "Q0", sent_id[0], sent_id[1], rank, scoreDocsArray[i].score, runID);
                        } else {
                            run.printf(Locale.ENGLISH, "%s\t%s\t%s\t%d\t%.6f\t%s%n", t.getQueryID(), "Q0", "NOT-ENOUGH", rank, scoreDocsArray[i].score, runID);
                        }
                    }
                    else{
                        run.printf(Locale.ENGLISH, "%s\t%s\t%s\t%d\t%.6f\t%s%n", t.getQueryID(), "Q0", docID, rank, scoreDocsArray[i].score, runID);
                    }
                    // Desired format: "t.getQueryID" "STANCE" "PA,IR" "RANK(i)" "SCORE" "RUNID"
                }

                run.flush();

            }
        } finally {
            run.close();

            reader.close();
        }

        elapsedTime = System.currentTimeMillis() - start;

        System.out.printf("%d topic(s) searched in %d seconds.", topics.size(), elapsedTime / 1000);

        System.out.printf("#### Searching complete ####%n");
    }

    public static void main(String[] args) throws Exception {

//        final String topics = "/Users/giovanni/uni/se22/topics.xml";
        final String topics = "/Users/giovanni/uni/se22/topics51-100.xml";


        // Experimental indexes paths
//        final String indexPath = "/Users/giovanni/uni/se22/experiment/index-ns-nocontext-bm25";
//        final String indexPath = "/Users/giovanni/uni/se22/experiment/index-nonstop2";
//        final String indexPath = "/Users/giovanni/uni/se22/experiment/index-lmdir";
//        final String indexPath = "/Users/giovanni/uni/se22/experiment/index-multi";
//        final String indexPath = "/Users/giovanni/uni/se22/experiment/NFSR-k-ldd-kstem-lmdir";
        final String indexPath = "/Users/giovanni/uni/se22/experiment/kstem-lmdir-nosent";


        final String runPath = "/Users/giovanni/uni/se22/experiment/";

        final String runID = "nosent-prem-1";

        final int maxDocsRetrieved = 20;

        final Analyzer a = CustomAnalyzer.builder()
                .withTokenizer(StandardTokenizerFactory.class)
                .addTokenFilter(LowerCaseFilterFactory.class)
                .addTokenFilter(StopFilterFactory.class)
                .build();
        final Analyzer b = CustomAnalyzer.builder()
                .withTokenizer(StandardTokenizerFactory.class)
                .addTokenFilter(KStemFilterFactory.class)
                .addTokenFilter(LowerCaseFilterFactory.class)
                .build();

        ArgsSearcher s = new ArgsSearcher(b, new LMDirichletSimilarity(), indexPath, topics, 50, runID, runPath, maxDocsRetrieved);

        s.search();


    }

}

