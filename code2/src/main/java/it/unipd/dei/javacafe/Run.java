package it.unipd.dei.javacafe;
import it.unipd.dei.javacafe.index.ArgsIndexer;
import it.unipd.dei.javacafe.parse.ArgsParser;
import it.unipd.dei.javacafe.search.ArgsSearcher;
import org.apache.commons.cli.*;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.StopFilterFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.en.EnglishPossessiveFilter;
import org.apache.lucene.analysis.en.EnglishPossessiveFilterFactory;
import org.apache.lucene.analysis.en.KStemFilterFactory;
import org.apache.lucene.analysis.en.PorterStemFilterFactory;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.search.similarities.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Run {
    public static void main(String[] args) throws IOException {
        // Command line interface initialization
        CommandLine cli;
        Options options = new Options();
        options.addOption("cp","corpusPath",
                true,"Path to the Args.me dataset");
        options.addOption("o","outputDir",
                true,"Path to the target directory where the index will be created");
        options.addOption("ip","indexPath",
                true, "Path to the index directory");
        options.addOption("tp","topicsPath",
                true, "Path to the topic list to use as queries");
        options.addOption("rd","runDir",
                true, "Directory where the run will be saved");
        options.addOption("n","maxDocNum",
                true, "Maximum number of documents to retrieve");
        options.addOption("I","INDEX",
                false, "Create a new index");
        options.addOption("S","SEARCH",
                false, "Search the index");
        options.addOption("IS","INDEXSEARCH",
                false, "Create a new index and search it");
        options.addOption("A","analyzer",
                true, "Set analyzer option");
        options.addOption("ID","runID",
                true, "Set an identifier for the run");
        options.addOption("sim","similarity",
                true, "Set similarity function options");
        options.addOption("rep","skipRep",
                true, "Select whether to skip indexing of documents with a repeated id or not.");
        options.addOption("sent","skipSent",
                true, "Select whether to skip indexing of sentences.");
        options.addOption("prem","skipLongPrem",
                true, "Select whether to skip indexing of long premises.");
        CommandLineParser parser = new DefaultParser();
        // Parsing of the arguments from Command Line
        try{
            cli = parser.parse(options, args);
        }catch (Exception e){
            e.printStackTrace();
            return;
        }

        // variable initialization
        String analyzerSettings = cli.hasOption("analyzer") ? cli.getOptionValue("analyzer") : "kstem";
        final Analyzer analyzer = getAnalyzer(analyzerSettings);
        String similaritySettings = cli.hasOption("similarity") ? cli.getOptionValue("similarity") : "lmdir";
        ArrayList<Similarity> parsedSim = getSimilarity(similaritySettings);
        final Similarity similarity = parsedSim.size()>1 ? new MultiSimilarity(parsedSim.toArray(new Similarity[]{})) : parsedSim.get(0);

        /* DEPRECATED
        List<Similarity> similarityList = new ArrayList<>();
        similarityList.add(new LMDirichletSimilarity());
        similarityList.add(new BM25Similarity());
        final Similarity similarity = new MultiSimilarity(similarityList.toArray(new Similarity[2]));
        */

        // Run initialization
        if(cli.hasOption("I") || cli.hasOption("IS")){
            final String docsPath = cli.getOptionValue("corpusPath");
            final String indexPath = cli.getOptionValue("outputDir");
            final int ramBuffer = 512;
            final String charsetName = "UTF-8";
            final long expectedDocs = 365408;
            boolean skipIds = true;
            boolean skipSent = true;
            boolean skipLongPrem = true;
            if(cli.hasOption("rep")){
                String cliOpt = cli.getOptionValue("rep");
                skipIds = cliOpt.toUpperCase().contains("Y");
            }
            if(cli.hasOption("sent")){
                String cliOpt = cli.getOptionValue("sent");
                skipSent = cliOpt.toUpperCase().contains("Y");
            }
            if(cli.hasOption("prem")){
                String cliOpt = cli.getOptionValue("prem");
                skipLongPrem = cliOpt.toUpperCase().contains("Y");
            }
            final ArgsIndexer indexer = new ArgsIndexer(analyzer, similarity, ramBuffer, indexPath, docsPath,
                    charsetName, expectedDocs, ArgsParser.class);
            indexer.setRepeatedIdsBehaviour(skipIds);
            indexer.setSkipSentences(skipSent);
            indexer.setSkipLongPremises(skipLongPrem);
            indexer.index();
        }

        if(cli.hasOption("S") || cli.hasOption("IS")){
            final String indexPath = cli.getOptionValue("indexPath");
            final String topicsPath = cli.getOptionValue("topicsPath");
            final String runDir = cli.getOptionValue("runDir");
            final String runID = cli.getOptionValue("runID");
            final int expectedTopics = 50;
            final int maxDocsRetrieved = Integer.parseInt(cli.getOptionValue("maxDocNum"));
            final ArgsSearcher searcher = new ArgsSearcher(analyzer,similarity,indexPath,topicsPath,
                    expectedTopics,runID,runDir,maxDocsRetrieved);
            try{
                searcher.search();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    private static Analyzer getAnalyzer(String arg) throws IOException{
        CustomAnalyzer.Builder out = CustomAnalyzer.builder()
                .withTokenizer(StandardTokenizerFactory.class)
                .addTokenFilter(LowerCaseFilterFactory.class);
        if(arg.contains("kstem")){
            out.addTokenFilter(KStemFilterFactory.class);
        }
        if(arg.contains("porter")){
            out.addTokenFilter(PorterStemFilterFactory.class);
        }
        if(arg.contains("stop")){
            out.addTokenFilter(StopFilterFactory.class);
        }
        if(arg.contains("len")){
            Map<String,String> params = new HashMap<>();
            params.put("min","3");
            params.put("max","20");
            out.addTokenFilter(LengthFilterFactory.class,params);
        }
        if(arg.contains("engpos")){
            out.addTokenFilter(EnglishPossessiveFilterFactory.class);
        }
        return out.build();
    }
    private static ArrayList<Similarity> getSimilarity(String arg) throws IOException{
        ArrayList<Similarity> returnValue = new ArrayList<>();
        boolean ok = false;
        if(arg.contains("multi")){
            returnValue.add(new LMDirichletSimilarity());
            returnValue.add(new BM25Similarity());
            ok = true;
        }
        if(arg.contains("lmdir")){
            try{
                int mu = Integer.parseInt(arg.substring(5));
                if(mu < 0 || mu > 10000){
                    throw new IllegalArgumentException("Illegal value for mu.");
                }
                returnValue.add(new LMDirichletSimilarity(mu));
            }catch (Exception e){
                returnValue.add(new LMDirichletSimilarity());
            }
            ok = true;
        }
        if(arg.contains("bm25")){
            returnValue.add(new BM25Similarity());
            ok = true;
        }
        if(!ok){
            throw new IOException("Provided similarity function options are not valid.");
        }
        return returnValue;
    }
}
