package it.unipd.dei.javacafe.index;

import it.unipd.dei.javacafe.parse.ArgsFields;
import it.unipd.dei.javacafe.parse.ArgsParser;
import it.unipd.dei.javacafe.parse.DocumentParser;
import it.unipd.dei.javacafe.parse.ParsedDocument;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.en.KStemFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class ArgsIndexer {

    private static final int MBYTE = 1024*1024;

    private final IndexWriter writer;
    private final Class<? extends DocumentParser> docParserClass;
    private final Path corpusPath;
    private final Charset cs;
    private final long expectedDocs;
    private final long start;
    private final HashMap<String, Long> repeatedIdsMap;
    private long filesCount;
    private long docsCount;
    private long bytesCount;
    private Set<String> docIdSet;
    private Set<String> voidTitleDocIdSet;
    private boolean skipRepeatedIds;
    private boolean skipSentences;
    private boolean skipLongPremises;

    public ArgsIndexer(final Analyzer analyzer, final Similarity similarity, final int ramBufferSizeMB,
                       final String indexPath, final String docsPath, final String charsetName,
                       final long expectedDocs, final Class<? extends DocumentParser> docParserClass) {
        if(docParserClass == null){
            throw new NullPointerException("Document parser class cannot be null.");
        }
        this.docParserClass = docParserClass;
        if (analyzer == null) {
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (ramBufferSizeMB <= 0) {
            throw new IllegalArgumentException("RAM buffer size cannot be less than or equal to zero.");
        }

        final IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
        iwc.setSimilarity(similarity);
        iwc.setRAMBufferSizeMB(ramBufferSizeMB);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        iwc.setCommitOnClose(true);
        iwc.setUseCompoundFile(true);

        if (indexPath == null) {
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }
        final Path indexDir = Paths.get(indexPath);
        if (Files.notExists(indexDir)) {
            try {
                Files.createDirectory(indexDir);
            } catch (Exception e) {
                throw new IllegalArgumentException(
                        String.format("Unable to create directory %s: %s.", indexDir.toAbsolutePath().toString(),
                                e.getMessage()), e);
            }
        }

        if (docsPath == null) {
            throw new NullPointerException("Documents path cannot be null.");
        }

        if (docsPath.isEmpty()) {
            throw new IllegalArgumentException("Documents path cannot be empty.");
        }

        final Path docsDir = Paths.get(docsPath);
        if (!Files.isReadable(docsDir)) {
            throw new IllegalArgumentException(
                    String.format("Documents path %s cannot be read.", docsDir.toAbsolutePath().toString()));
        }

        this.corpusPath = docsDir;

        if (charsetName == null) {
            throw new NullPointerException("Charset name cannot be null.");
        }

        if (charsetName.isEmpty()) {
            throw new IllegalArgumentException("Charset name cannot be empty.");
        }

        try {
            cs = Charset.forName(charsetName);
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("Unable to create the charset %s: %s.", charsetName, e.getMessage()), e);
        }

        if (expectedDocs <= 0) {
            throw new IllegalArgumentException(
                    "The expected number of documents to be indexed cannot be less than or equal to zero.");
        }
        this.expectedDocs = expectedDocs;

        this.docsCount = 0;

        this.bytesCount = 0;

        this.filesCount = 0;

        this.docIdSet = new HashSet<>();
        this.voidTitleDocIdSet = new HashSet<>();
        this.repeatedIdsMap = new HashMap<>();
        this.skipRepeatedIds = true;
        this.skipSentences = true;
        try {
            writer = new IndexWriter(FSDirectory.open(indexDir), iwc);
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index writer in directory %s: %s.",
                    indexDir.toAbsolutePath().toString(), e.getMessage()), e);
        }

        this.start = System.currentTimeMillis();
    }

    public void index() throws IOException{
        System.out.printf("%n#### Start indexing ####%n");

        DocumentParser docParser = DocumentParser.create(docParserClass, Files.newBufferedReader(corpusPath,cs));

        bytesCount += Files.size(corpusPath);
        filesCount += 1;
        Document doc = null;

        for(ParsedDocument parsedDocument : docParser){
            if(parsedDocument==null){
                continue;
            }
            if(docIdSet.contains(parsedDocument.getId())){
                long val = repeatedIdsMap.getOrDefault(parsedDocument.getId(), 1L);
                repeatedIdsMap.put(parsedDocument.getId(), val+1L);
                if(skipRepeatedIds){
                    continue;
                }
            }
            doc = new Document();

            doc.add(new StringField(ParsedDocument.FIELDS.ID,
                    parsedDocument.getId(),
                    Field.Store.YES));

            docIdSet.add(parsedDocument.getId());
            if(parsedDocument.getContext().get(ArgsFields.CONTEXT.DISCUSSION_TITLE) != null){
                doc.add(new TitleField(parsedDocument.getContext().get(ArgsFields.CONTEXT.DISCUSSION_TITLE).toString()));
            }
            else {
                voidTitleDocIdSet.add(parsedDocument.getId());
            }

            doc.add(new BodyTextField(
                    ParsedDocument.FIELDS.CONCLUSION,
                    parsedDocument.getConclusion()));

            if(parsedDocument.getContext().get(ArgsFields.CONTEXT.TOPIC) != null){
                doc.add(new TopicField(parsedDocument.getContext().get(ArgsFields.CONTEXT.TOPIC).toString()));
            }
            for(Map<String,?> sentence : parsedDocument.getSentences()) {
                String sent_id =  sentence.get(ArgsFields.SENTENCES.SENT_ID).toString();
                String sent_text =  sentence.get(ArgsFields.SENTENCES.SENT_TEXT).toString();
                if(!skipSentences){
                    doc.add(new StringField(ArgsFields.SENTENCES.SENT_ID,
                            sent_id,
                            Field.Store.YES));
                    doc.add(new BodyTextField(ArgsFields.SENTENCES.SENT_TEXT,
                            sent_text));
                }
            }
            for(Map<String,?> premise : parsedDocument.getPremises()) {
                String text =  premise.get(ArgsFields.PREMISES.TEXT).toString();
                String stance =  premise.get(ArgsFields.PREMISES.STANCE).toString();
                if(skipLongPremises && (text.length() < 20 || text.length() > 10000)){
                    continue;
                }
                doc.add(new BodyTextField(ArgsFields.PREMISES.TEXT, text));
                doc.add(new StanceField(stance));
            }
            writer.addDocument(doc);
            docsCount++;
            if (docsCount % 10000 == 0) {
                System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n",
                        docsCount, filesCount, bytesCount / MBYTE,
                        (System.currentTimeMillis() - start) / 1000);
            }
        }
        writer.commit();
        writer.close();
        if (docsCount != expectedDocs) {
            System.out.printf("Expected to index %d documents; %d indexed instead.%n", expectedDocs, docsCount);
        }

        String path = "/Users/giovanni/uni/se22/experiment/voidIds.txt";
        try{
            Path voidIdsFile = Path.of(path);
            if(Files.notExists(voidIdsFile)){
                Files.createFile(voidIdsFile);
            }
            BufferedWriter writer = Files.newBufferedWriter(voidIdsFile);
            for(String key : voidTitleDocIdSet){
                writer.write(key);
                writer.newLine();
            }
            writer.close();
        }catch (Exception e){
            System.out.println("Error while writing void title document ids to file. : " + path);
        }
        String pathRep = "/Users/giovanni/uni/se22/experiment/repIds.txt";
        try{
            Path repIdsFile = Path.of(pathRep);
            if(Files.notExists(repIdsFile)){
                Files.createFile(repIdsFile);
            }
            BufferedWriter writer = Files.newBufferedWriter(repIdsFile);
            for(String key : repeatedIdsMap.keySet()){
                writer.write(key);
                writer.write("\t");
                writer.write(repeatedIdsMap.get(key).toString());
                writer.newLine();
            }
            writer.close();
        }catch (Exception e){
            System.out.println("Error while writing void title document ids to file. : " + pathRep);
        }
        System.out.printf("%d document(s) (%d files, %d Mbytes) indexed over %d distinct ids in %d seconds.%n", docsCount, filesCount, docIdSet.size(),
                bytesCount / MBYTE, (System.currentTimeMillis() - start) / 1000);
        int repIdsNum = repeatedIdsMap
                .values()
                .stream()
                .mapToInt(Long::intValue).sum();
        System.out.printf("%d document(s) with non unique ids found in the corpus.%n", repIdsNum);
        String toAddOrNotToAdd = skipRepeatedIds ? "were not" : "were";
        System.out.printf("%d document(s) with an already indexed id %s added to the index %n",repIdsNum-repeatedIdsMap.keySet().size(),toAddOrNotToAdd);
        System.out.printf("#### Indexing complete ####%n");
    }
    public void setRepeatedIdsBehaviour(boolean skipRepeatedIds){
        this.skipRepeatedIds = skipRepeatedIds;
    }
    public void setSkipSentences(boolean skipSentences){
        this.skipSentences = skipSentences;
    }
    public static void main(String[] args) throws Exception {

        final int ramBuffer = 512;
        final String docsPath = "/Users/giovanni/uni/se22/args_processed.csv";
        final String indexPath = "/Users/giovanni/uni/se22/experiment/nosent";

        final int expectedDocs = 365408;
        final String charsetName = "UTF-8";

        final Analyzer a = CustomAnalyzer.builder()
                .withTokenizer(StandardTokenizerFactory.class)
                .addTokenFilter(LowerCaseFilterFactory.class)
                .build();
        final Analyzer b = CustomAnalyzer.builder()
                .withTokenizer(StandardTokenizerFactory.class)
                .addTokenFilter(KStemFilterFactory.class)
                .addTokenFilter(LowerCaseFilterFactory.class)
                .build();
        Similarity similarity = new LMDirichletSimilarity();
        Similarity similarity2 = new BM25Similarity();
        List<Similarity> similarityList = new ArrayList<>();
        similarityList.add(similarity);
        similarityList.add(similarity2);
        Similarity[] similarities = similarityList.toArray(new Similarity[2]);
        ArgsIndexer i = new ArgsIndexer(b, similarity2, ramBuffer, indexPath, docsPath,
                charsetName, expectedDocs, ArgsParser.class);

        i.index();
    }

    public void setSkipLongPremises(boolean skipLongPrem) {
        this.skipLongPremises = skipLongPrem;
    }
}
