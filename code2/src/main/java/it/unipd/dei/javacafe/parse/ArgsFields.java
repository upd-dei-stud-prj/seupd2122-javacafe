package it.unipd.dei.javacafe.parse;

public class ArgsFields {
    public final static class CONTEXT{
        public final static String ACQUISITION_TIME = "acquisitionTime";
        public final static String ASPECTS = "aspects";
        public final static String DISCUSSION_TITLE = "discussionTitle";
        public final static String MODE = "mode";
        public final static String SOURCE_DOMAIN = "sourceDomain";
        public final static String SOURCE_ID = "sourceId";
        public final static String SOURCE_TEXT = "sourceText";
        public final static String SOURCE_TEXT_CONCLUSION_START = "sourceTextConclusionStart";
        public final static String SOURCE_TEXT_CONCLUSION_END = "sourceTextConclusionEnd";
        public final static String SOURCE_TEXT_PREMISE_START = "sourceTextPremiseStart";
        public final static String SOURCE_TEXT_PREMISE_END = "sourceTextPremiseEnd";
        public final static String SOURCE_TITLE = "sourceTitle";
        public final static String SOURCE_URL = "sourceUrl";
        public final static String TOPIC = "topic";
        public final static class ASPECTS_FIELDS{
            public final static String NAME = "name";
            public final static String WEIGHT = "weight";
            public final static String NORMALIZED_WEIGHT = "normalizedWeight";
            public final static String RANK = "rank";
        }
    }
    public final static class PREMISES{
        public final static String TEXT = "text";
        public final static String STANCE = "stance";
        public final static String ANNOTATIONS = "annotations";
    }
    public final static class SENTENCES{
        public final static String SENT_TEXT = "sent_text";
        public final static String SENT_ID = "sent_id";
    }
}
