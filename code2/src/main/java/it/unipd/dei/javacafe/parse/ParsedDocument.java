package it.unipd.dei.javacafe.parse;

import org.apache.lucene.document.Field;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;
import java.util.Map;

/**
 * Represents a parsed document to be indexed.
 *
 * @author Giovanni Foti (giovanni.foti@studenti.unidp.it)
 * @version 1.00
 * @since 1.00
 */
public class ParsedDocument {

    /**
     * The names of the {@link Field}s within the index.
     *
     * @author Giovanni Foti (giovanni.foti@studenti.unidp.it)
     * @version 1.00
     * @since 1.00
     */
    public final static class FIELDS {

        public static final String ID = "id";

        public static final String CONCLUSION = "conclusion";

        public static final String PREMISES = "premises";

        public static final String CONTEXT = "context";

        public static final String SENTENCES = "sentences";


    }

    private final String id;
    private final String conclusion;
    private final List<Map<String,?>> premises;
    private final Map<String,?> context;
    private final List<Map<String,?>> sentences;

    public ParsedDocument(final String id, final String conclusion,final List<Map<String,?>> premises,final Map<String,?> context,final List<Map<String,?>> sentences) {
        if(id == null){
            throw new NullPointerException("Document identifier cannot be null.");
        }
        if(id.isEmpty()){
            throw new NullPointerException("Document identifier cannot be empty.");
        }

        this.id = id;
        this.conclusion = conclusion;
        this.premises = premises;
        this.context = context;
        this.sentences = sentences;
    }

    /**
     * Returns the unique document identifier.
     *
     * @return the unique document identifier.
     */
    public String getIdentifier() {
        return id;
    }

    public String getId() {
        return id;
    }

    public List<Map<String,?>> getSentences() {
        return sentences;
    }

    public Map<String,?> getContext() {
        return context;
    }

    public List<Map<String,?>> getPremises() {
        return premises;
    }

    public String getConclusion() {
        return conclusion;
    }

    @Override
    public final String toString() {
        ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("identifier", id)
                .append("conclusion", conclusion)
                .append("premises", premises)
                .append("context", context)
                .append("sentences", sentences);
        return toStringBuilder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return (this == obj) || ((obj instanceof ParsedDocument) && id.equals(((ParsedDocument) obj).id));
    }

    @Override
    public final int hashCode() {
        return 41 * super.hashCode();
    }
}
