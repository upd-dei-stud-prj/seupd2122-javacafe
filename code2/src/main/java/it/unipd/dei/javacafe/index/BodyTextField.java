package it.unipd.dei.javacafe.index;

import it.unipd.dei.javacafe.parse.ArgsFields;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

import java.io.Reader;

public class BodyTextField extends Field {

    /**
     * The type of the document body field
     */
    private static final FieldType TYPE = new FieldType();

    static {
        TYPE.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        TYPE.setTokenized(true);
        TYPE.setOmitNorms(false);
        TYPE.setStored(false);
    }


    /**
     * Create a new field for the body of a document.
     *
     * @param value the contents of the body of a document.
     */
    public BodyTextField(final String name, final Reader value) {
        super(name, value, TYPE);
    }

    /**
     * Create a new field for the body of a document.
     *
     * @param value the contents of the body of a document.
     */
    public BodyTextField(final String name,final String value) {
        super(name, value, TYPE);
    }

}
