package it.unipd.dei.javacafe.parse;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

public class ArgsParser extends DocumentParser {

    private ParsedDocument document = null;

    private final ObjectMapper documentMapper;

    public ArgsParser(final Reader in) {
        super(new BufferedReader(in));
        documentMapper = JsonMapper.builder()
                .configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true)
                .enable(JsonReadFeature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER)
                .build();
    }

    @Override
    public boolean hasNext() {
        MappingIterator<Map<String, String>> it = null;
        Map<String,String> rowAsMap = null;
        List<Map<String,?>> premisesAsListedMaps = null;
        Map<String,?> contextAsMap = null;
        List<Map<String,?>> sentencesAsListedMaps = null;
        String id = null;
        String conclusion = null;
        String premises = null;
        String sentences = null;
        String context = null;
        String line = null;
        long lineno = 0;
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.builder()
                .addColumn(ParsedDocument.FIELDS.ID)
                .addColumn(ParsedDocument.FIELDS.CONCLUSION)
                .addColumn(ParsedDocument.FIELDS.PREMISES)
                .addColumn(ParsedDocument.FIELDS.CONTEXT)
                .addColumn(ParsedDocument.FIELDS.SENTENCES)
                .setColumnSeparator(',')
                .build();
        try {
            line = ((BufferedReader) in).readLine();
            if(line == null || line.isEmpty()){
                next = false;
                return next;
            }
            if(line.startsWith("id,")){
                return next;
            }
            it = mapper.readerFor(Map.class)
                    .with(schema)
                    .readValues(new StringReader(line));

            if(it != null && it.hasNext()){
                rowAsMap = it.next();
                // Read String values
                id = rowAsMap.get(ParsedDocument.FIELDS.ID);
                conclusion = rowAsMap.get(ParsedDocument.FIELDS.CONCLUSION);
                premises = rowAsMap.get(ParsedDocument.FIELDS.PREMISES);
                context = rowAsMap.get(ParsedDocument.FIELDS.CONTEXT);
                sentences = rowAsMap.get(ParsedDocument.FIELDS.SENTENCES);
                // Parse {field}As{DataStructure} variables
                premisesAsListedMaps = documentMapper.readValue(premises, new TypeReference<List<Map<String, ?>>>() {});
                sentencesAsListedMaps = documentMapper.readValue(sentences, new TypeReference<List<Map<String, ?>>>() {});
                contextAsMap = documentMapper.readValue(context, new TypeReference<Map<String, ?>>() {});

                if(premisesAsListedMaps!= null && premisesAsListedMaps.size()!=1){
                    System.out.println("------------------------MORE THAN ONE1111--------------");
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        if (id != null) {
            document = new ParsedDocument(id, conclusion, premisesAsListedMaps, contextAsMap, sentencesAsListedMaps);
        }
        return next;
    }

    private String extractPremisesText(List<Map<String,?>> premisesAsListedMaps){
        Map<String,?> mappedPremises = null;
        if(premisesAsListedMaps != null && premisesAsListedMaps.size() == 1){
            mappedPremises = premisesAsListedMaps.get(0);
            return mappedPremises.get(ArgsFields.PREMISES.TEXT).toString();
        }
        return null;
    }

    private String extractPremisesStance(List<Map<String,?>> premisesAsListedMaps){
        Map<String,?> mappedPremises = null;
        if(premisesAsListedMaps != null && premisesAsListedMaps.size() == 1){
            mappedPremises = premisesAsListedMaps.get(0);
            return mappedPremises.get(ArgsFields.PREMISES.STANCE).toString();
        }
        return null;
    }

    private String extractSentenceID(Map<String,?> sentenceAsMap){
        if(sentenceAsMap != null){
            return sentenceAsMap.get(ArgsFields.SENTENCES.SENT_ID).toString();
        }
        return null;
    }

    private String extractSentenceText(Map<String,?> sentenceAsMap){
        if(sentenceAsMap != null){
            return sentenceAsMap.get(ArgsFields.SENTENCES.SENT_TEXT).toString();
        }
        return null;
    }

    private String extractContextSourceText(Map<String, ?> mappedContext){
        String retval = null;
        try {
            retval = ((String) mappedContext.get(ArgsFields.CONTEXT.SOURCE_TEXT));
        }catch (Exception e){
            e.printStackTrace();
        }
        retval = retval==null ? "" : retval;
        return retval;
    }

    private String extractContextDiscussionTitle(Map<String, ?> mappedContext){
        String retval = null;
        try {
            retval = ((String) mappedContext.get(ArgsFields.CONTEXT.DISCUSSION_TITLE));
        }catch (Exception e){
            e.printStackTrace();
        }
        retval = retval==null ? "" : retval;
        return retval;
    }

    @Override
    protected ParsedDocument parse() {
        return document;
    }

    public static void main(String[] args) throws Exception{
        String filename = "/Users/giovanni/uni/se22/args_processed.csv";
        Reader reader = new FileReader(filename);

        ArgsParser parser = new ArgsParser(reader);
        long i = 0;
        long nonnull = 0;
        for( ParsedDocument doc : parser){
            i++;
            if(doc!=null){
                nonnull++;
//                System.out.printf("%n%n------------------------------------%n%s%n%n%n", doc.toString());
            }
        }
        System.out.println("\n\n------------------------------------\nTOT= " + Long.toString(i)+ "  NONNULL= " +Long.toString(nonnull) + "\n\n\n" );
    }
}