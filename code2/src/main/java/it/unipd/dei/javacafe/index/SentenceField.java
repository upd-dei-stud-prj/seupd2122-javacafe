package it.unipd.dei.javacafe.index;

import it.unipd.dei.javacafe.parse.ParsedDocument;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

import java.io.Reader;

public class SentenceField extends Field {
    private static final FieldType SENTENCE_TYPE = new FieldType();
    static {
        SENTENCE_TYPE.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        SENTENCE_TYPE.setTokenized(true);
        SENTENCE_TYPE.setStored(true);
        SENTENCE_TYPE.setOmitNorms(true);
    }

    public SentenceField(final Reader value) {
        super(ParsedDocument.FIELDS.CONTEXT, value, SENTENCE_TYPE);
    }

    public SentenceField(final String value) {
        super(ParsedDocument.FIELDS.CONTEXT, value, SENTENCE_TYPE);
    }

}
