package it.unipd.dei.javacafe.index;

import it.unipd.dei.javacafe.parse.ArgsFields;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

import java.io.Reader;

public class StanceField extends Field {

    /**
     * The type of the document body field
     */
    private static final FieldType TYPE = new FieldType();

    static {
        TYPE.setIndexOptions(IndexOptions.DOCS_AND_FREQS);
        TYPE.setTokenized(false);
        TYPE.setOmitNorms(true);
        TYPE.setStored(true);
    }


    /**
     * Create a new field for the body of a document.
     *
     * @param value the contents of the body of a document.
     */
    public StanceField(final Reader value) {
        super(ArgsFields.CONTEXT.DISCUSSION_TITLE, value, TYPE);
    }

    /**
     * Create a new field for the body of a document.
     *
     * @param value the contents of the body of a document.
     */
    public StanceField(final String value) {
        super(ArgsFields.CONTEXT.DISCUSSION_TITLE, value, TYPE);
    }

}
