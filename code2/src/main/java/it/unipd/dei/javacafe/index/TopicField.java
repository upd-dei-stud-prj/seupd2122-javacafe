package it.unipd.dei.javacafe.index;

import it.unipd.dei.javacafe.parse.ArgsFields;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

import java.io.Reader;

public class TopicField extends Field {

    /**
     * The type of the document body field
     */
    private static final FieldType TOPIC_TYPE = new FieldType();

    static {
        TOPIC_TYPE.setIndexOptions(IndexOptions.DOCS_AND_FREQS);
        TOPIC_TYPE.setTokenized(false);
        TOPIC_TYPE.setOmitNorms(true);
        TOPIC_TYPE.setStored(true);
    }


    /**
     * Create a new field for the body of a document.
     *
     * @param value the contents of the body of a document.
     */
    public TopicField(final Reader value) {
        super(ArgsFields.CONTEXT.TOPIC, value, TOPIC_TYPE);
    }

    /**
     * Create a new field for the body of a document.
     *
     * @param value the contents of the body of a document.
     */
    public TopicField(final String value) {
        super(ArgsFields.CONTEXT.TOPIC, value, TOPIC_TYPE);
    }

}
